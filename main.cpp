// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#include <lua.hpp>
#include "luag.h"
#include "parse.h"
#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

extern void point3d_register( lua_State *L );
extern void vector3d_register( lua_State *L );
extern void pgn_register( lua_State *L );
extern void shp_register( lua_State *L );
extern void wp_register( lua_State *L );

unsigned short curve_point_count = 1;
static char const* current_lua_source = 0;

//
// Lua Functions for global scope
//

static int set_point_count(lua_State *L)
{
	luaG_checkargcount(L,1);
	lua_Integer pc = luaL_checkinteger(L,1);
	luaL_argcheck(L, pc>0, 1, "positive integer expected" );
	
	curve_point_count = pc;
	
	return 0;
}

static int get_point_count(lua_State *L)
{
	luaG_checkargcount(L,0);
	lua_pushnumber(L,curve_point_count);
	return 1;
}

static int basename(lua_State *L)
{
	luaG_checkargcount(L,0);
	lua_pushstring(L,current_lua_source);
	return 1;
}

//
// Application framework
//

static int process_lua_source( char const* filename )
{
	lua_State* L;

#if LUA_VERSION_NUM > 501
	L = luaL_newstate();
#else
	L = lua_open();
#endif
	luaopen_base(L);
	luaopen_string(L);
	luaopen_math(L);
	lua_register( L, "set_point_count", set_point_count );
	lua_register( L, "get_point_count", get_point_count );
	lua_register( L, "basename", basename );
	point3d_register(L);
	vector3d_register(L);
	pgn_register( L );
	shp_register(L );
	wp_register(L);
	
	current_lua_source = filename;
	int rc = luaL_dofile( L, filename );
	if ( rc!=0 ) {
		switch(rc) {
			case LUA_ERRRUN: printf("Runtime error.\n"); break;
			case LUA_ERRSYNTAX: printf("Syntax error.\n"); break;
			case LUA_ERRMEM: printf("Memory allocation error.\n"); break;
			case LUA_ERRERR: printf("Error in error handlers.\n"); break;
			default: printf("Unknown error (%d).\n",rc); break;
		}
		printf( "%s\n", lua_tostring(L,-1) );
	}

	lua_close(L);
	return rc==0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

static void print_usage( FILE* out )
{
	fprintf( out,
	"Usage for grivvin:\n"
	"\tgrivvin [options] filename [ [options] filename ...]\n"
	"\tgrivvin -h\tRequest help.  Print this message.\n"
	"\tgrivvin -v\tRequest version information.\n"
	"\nOptions:\n"
	"\t-p [integer]\tset number of points used to define each 90o curve\n"
	"\t-v [bool]\tflag to turn on verbose behaviour.\n" );
}

static void print_version( FILE* out )
{
	fprintf( out,
	"Version information for grivvin:\n"
	"\tgrivvin:\tdevelopment\n"
	"\tCGAL:\t???\n" );
}

int main(int argc, char* argv[])
{
	// check that we have at least one parameter, this is in 
	// addition to the program name
	if ( argc<2 ) 
	{
		print_usage( stdout );
		return EXIT_FAILURE;
	}
	
	// if we have exactly two arguments, we might be requesting
	// either help or version information 
	if ( argc==2 && argv[1][0]=='-' )
	{
		// there is one command-line argument
		// that argument is a command switch
		
		// first, ignore first dash if there are two
		if ( argv[1][1]=='-' ) ++argv[1];
		assert( argv[1][0]=='-' );
		
		// is it a request for help?
		if ( tolower( argv[1][1] ) == 'h' )
		{
			print_usage( stdout );
			return EXIT_SUCCESS;
		}
		
		// is it a request for version information?
		if ( tolower( argv[1][1] ) == 'v' )
		{
			print_version( stdout );
			return EXIT_SUCCESS;
		}
		
	}
	
	// skip the program name
	++argv;
	// get command line options
	while ( *argv ) 
	{
		// to simplify this loop
		// store the string currently being processed
		char const* str = *argv;

		// check if the current string contains
		// an option to be processed
		if ( str[0] == '-' ) 
		{
			// only the first character after the dash
			// is significant in determining what option
			// is being set
			switch ( tolower(str[1]) )
			{
			case 'p':
				++argv;
				if ( parse_ushort( &curve_point_count, *argv, 0, 19 ) ) {
					fprintf( stderr, "ERROR:  The curve point count must be an integer between 0 and 19.\n" );
					return EXIT_FAILURE;
				}
				break;
				
			default:
				fprintf( stderr, "Unknown argument for application.\n\n" );
				print_usage( stderr );
				return EXIT_FAILURE;
			}
		}
		else 
		{
			// we are going to process a file
			int rc = process_lua_source( *argv );
			if ( rc!=0 ) return EXIT_FAILURE;
		}

		// move to next argument
		++argv;
	}

	// processing is complete
	return EXIT_SUCCESS;
}
