// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include <cassert>
#include <lua.hpp>
#include "luag.h"
#include "grivvin.h"

using grivvin::vector3d;

template <>
char const* const luaG_traits<vector3d>::metatable = "grivvin.vector3d";

static int vector3d_new(lua_State *L) 
{
	luaG_checkargcount( L, 3 );
	lua_Number x = luaL_checknumber( L, 1 );
	lua_Number y = luaL_checknumber( L, 2 );
	lua_Number z = luaL_checknumber( L, 3 );
	
	luaG_push( L, vector3d( x, y, z ) );
	return 1;
}

static int vector3d_gc(lua_State *L)
{
	vector3d* ud = luaG_checkudata<vector3d>(L,1);
	assert( ud );
	ud->~vector3d();
	return 0;
}

static int vector3d_eq(lua_State *L)
{
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	vector3d* ud2 = luaG_checkudata<vector3d>(L,2);
	
	lua_pushboolean( L, *ud1 == *ud2 );
	return 1;
}

static int vector3d_add(lua_State *L)
{
	luaG_checkargcount( L, 2 );
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	vector3d* ud2 = luaG_checkudata<vector3d>(L,2);
	
	assert( ud1 && ud2 );
	luaG_push( L, *ud1 + *ud2 );
	return 1;
	
}

static int vector3d_sub(lua_State *L)
{
	luaG_checkargcount( L, 2 );
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	vector3d* ud2 = luaG_checkudata<vector3d>(L,2);
	
	assert( ud1 && ud2 );
	luaG_push( L, *ud1 - *ud2 );
	return 1;
	
}

static int vector3d_mul(lua_State *L)
{
	luaG_checkargcount( L, 2 );
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	lua_Number x = luaL_checknumber( L, 2 );
	
	assert( ud1 );
	luaG_push( L, *ud1 * x );
	return 1;
	
}

static int vector3d_div(lua_State *L)
{
	luaG_checkargcount( L, 2 );
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	lua_Number x = luaL_checknumber( L, 2 );
	
	assert( ud1 );
	luaG_push( L, *ud1 / x );
	return 1;
}

static int vector3d_tostring(lua_State *L)
{
	luaG_checkargcount( L, 1 );
	vector3d* ud = luaG_checkudata<vector3d>(L,1);
	char buffer[ (1+2+10+1+2)*3 + 4 + 1];
	
	int n = snprintf( buffer, sizeof(buffer), "(%g,%g,%g)", CGAL::to_double(ud->x()), CGAL::to_double(ud->y()), CGAL::to_double(ud->z()) );
	assert( n>=0 && static_cast<size_t>(n)<sizeof(buffer) );
	lua_pushstring(L,buffer);
	return 1;
}

static int vector3d_x(lua_State *L)
{
	luaG_checkargcount(L,1);
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	lua_pushnumber( L, CGAL::to_double( ud1->x() ) );
	return 1;
	
}

static int vector3d_y(lua_State *L)
{
	luaG_checkargcount(L,1);
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	lua_pushnumber( L, CGAL::to_double( ud1->y() ) );
	return 1;
	
}

static int vector3d_z(lua_State *L)
{
	luaG_checkargcount(L,1);
	vector3d* ud1 = luaG_checkudata<vector3d>(L,1);
	lua_pushnumber( L, CGAL::to_double( ud1->z() ) );
	return 1;
	
}

void vector3d_register( lua_State *L )
{
	luaL_newmetatable(L,luaG_traits<vector3d>::metatable);
	lua_pushstring(L, "__index");
	lua_pushvalue(L,-2); /* pushes the metatable */
	lua_settable(L,-3); /*metatable.__index = metatable */
	luaG_addfunction(L, "__gc", vector3d_gc );
	luaG_addfunction(L, "__eq", vector3d_eq );
	luaG_addfunction(L, "__add", vector3d_add );
	luaG_addfunction(L, "__sub", vector3d_sub );
	luaG_addfunction(L, "__mul", vector3d_mul );
	luaG_addfunction(L, "__div", vector3d_div );
	luaG_addfunction(L, "__tostring", vector3d_tostring );
	luaG_addfunction(L, "scale", vector3d_mul );
	luaG_addfunction(L, "x", vector3d_x );
	luaG_addfunction(L, "y", vector3d_y );
	luaG_addfunction(L, "z", vector3d_z );
	
	lua_register( L, "vector3d", vector3d_new );
}
