// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include <lua.hpp>
#include <cstring>
#include <memory>

#include "vendor/catch.hpp"

extern lua_State* init_lua_state();
extern int exec_lua_script( lua_State* L, char const* source );

TEST_CASE( "vector3d" )
{
	SECTION( "new" ) {
		char const* script =
			"v = vector3d(1,2,3)\n"
			"t = type(v)\n"
			"x = v:x()\n"
			"y = v:y()\n"
			"z = v:z()\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "t");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string("userdata") );
		lua_getglobal(L, "x");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 1 );
		lua_getglobal(L, "y");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 2 );
		lua_getglobal(L, "z");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 3 );
	}

	SECTION( "gc" ) {
		char const* script =
			"v = vector3d(1,2,3)\n"
			"v = nil\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		lua_gc( L, LUA_GCCOLLECT, 0 );
	}
	
	SECTION( "eq" ) {
		char const* script =
			"ret1 = ( vector3d(1,2,3) == vector3d(2,3,4) )\n"
			"ret2 = ( vector3d(1,2,3) ~= vector3d(2,3,4) )\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "ret1");
		CHECK( lua_isboolean(L,-1) );
		CHECK( !lua_toboolean(L,-1) );
		lua_getglobal(L, "ret2");
		CHECK( lua_isboolean(L,-1) );
		CHECK( lua_toboolean(L,-1) );
	}
	
	SECTION( "add" ) {
		char const* script =
			"v = vector3d(1,2,3) + vector3d(2,3,4)\n"
			"x = v:x()\n"
			"y = v:y()\n"
			"z = v:z()\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "x");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 3 );
		lua_getglobal(L, "y");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 5 );
		lua_getglobal(L, "z");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 7 );
	}

	SECTION( "sub" ) {
		char const* script =
			"v = vector3d(1,2,3) - vector3d(2,3.5,5)\n"
			"x = v:x()\n"
			"y = v:y()\n"
			"z = v:z()\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "x");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
		lua_getglobal(L, "y");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1.5 );
		lua_getglobal(L, "z");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -2 );
	}

	SECTION( "mul" ) {
		char const* script =
			"v = vector3d(1,2,3) * 2\n"
			"x = v:x()\n"
			"y = v:y()\n"
			"z = v:z()\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "x");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 2 );
		lua_getglobal(L, "y");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 4 );
		lua_getglobal(L, "z");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 6 );
	}

	SECTION( "div" ) {
		char const* script =
			"v = vector3d(1,2,3) / 2\n"
			"x = v:x()\n"
			"y = v:y()\n"
			"z = v:z()\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "x");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 0.5 );
		lua_getglobal(L, "y");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 1 );
		lua_getglobal(L, "z");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 1.5 );
	}

	SECTION( "scale" ) {
		char const* script =
			"v = vector3d(1,2,3):scale(3)\n"
			"x = v:x()\n"
			"y = v:y()\n"
			"z = v:z()\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "x");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 3 );
		lua_getglobal(L, "y");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 6 );
		lua_getglobal(L, "z");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 9 );
	}

	SECTION( "tostring" ) {
		char const* script =
			"v = vector3d(1.2,3.4,5.6)\n"
			"s = tostring(v)\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "s");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string("(1.2,3.4,5.6)") );
	}
}
