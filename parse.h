// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#ifndef _GRIVVIN_PARSE_H_
#define _GRIVVIN_PARSE_H_

int parse_long( long* var, char const* text, long min, long max );
int parse_short( short* var, char const* text, short min, short max );
int parse_ushort( unsigned short* var, char const* text, unsigned short min, unsigned short max );
int parse_bool( bool* var, char const* text );

#endif
