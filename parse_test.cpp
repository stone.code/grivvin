// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "parse.h"
#include <cerrno>

#include "vendor/catch.hpp"

TEST_CASE( "parse_long" )
{
	struct {
		char const* text;
		long min, max;
		long expected;
		int err;
	} cases[] = {
		{ "0", -10, 10, 0, 0 },
		{ "-10", -10, 10, -10, 0 },
		{ "10", -10, 10, 10, 0 },
		{ "-11", -10, 10, 0, ERANGE },
		{ "11", -10, 10, 0, ERANGE },
		{ "", -10, 10, 0, EINVAL },
		{ "zz", -10, 10, 0, EINVAL },
		{ "12345678901234567890", -10, 10, 0, ERANGE },
		{ 0, 0, 0, 0, 0}
	};
	
	for ( int i = 0; cases[i].text; ++i ) {
		INFO( "Case:" << i );

		long got = 0;
		int err = parse_long( &got, cases[i].text, cases[i].min, cases[i].max );
		CHECK( got == cases[i].expected );
		CHECK( err == cases[i].err );
	}
}

TEST_CASE( "parse_short" )
{
	struct {
		char const* text;
		short min, max;
		short expected;
		int err;
	} cases[] = {
		{ "0", -10, 10, 0, 0 },
		{ "-10", -10, 10, -10, 0 },
		{ "10", -10, 10, 10, 0 },
		{ "-11", -10, 10, 0, ERANGE },
		{ "11", -10, 10, 0, ERANGE },
		{ "", -10, 10, 0, EINVAL },
		{ "zz", -10, 10, 0, EINVAL },
		{ "12345678901234567890", -10, 10, 0, ERANGE },
		{ 0, 0, 0, 0, 0}
	};
	
	for ( int i = 0; cases[i].text; ++i ) {
		INFO( "Case:" << i );

		short got = 0;
		int err = parse_short( &got, cases[i].text, cases[i].min, cases[i].max );
		CHECK( got == cases[i].expected );
		CHECK( err == cases[i].err );
	}
}

TEST_CASE( "parse_ushort" )
{
	struct {
		char const* text;
		unsigned short min, max;
		unsigned short expected;
		int err;
	} cases[] = {
		{ "0", 0, 10, 0, 0 },
		{ "1", 0, 10, 1, 0 },
		{ "10", 0, 10, 10, 0 },
		{ "11", 0, 10, 0, ERANGE },
		{ "0", 1, 10, 0, ERANGE },
		{ "1", 1, 10, 1, 0 },
		{ "2", 1, 10, 2, 0 },
		{ "", -10, 10, 0, EINVAL },
		{ "zz", -10, 10, 0, EINVAL },
		{ "12345678901234567890", -10, 10, 0, ERANGE },
		{ 0, 0, 0, 0, 0}
	};
	
	for ( int i = 0; cases[i].text; ++i ) {
		INFO( "Case:" << i );

		unsigned short got = 0;
		int err = parse_ushort( &got, cases[i].text, cases[i].min, cases[i].max );
		CHECK( got == cases[i].expected );
		CHECK( err == cases[i].err );
	}
}

TEST_CASE( "parse_bool" )
{
	struct {
		char const* text;
		bool expected;
		int err;
	} cases[] = {
		{ "0", false, 0 },
		{ "1", true, 0 },
		{ "f", false, 0 },
		{ "t", true, 0 },
		{ "false", false, 0 },
		{ "true", true, 0 },
		{ "no", false, 0 },
		{ "yes", true, 0 },
		{ "", false, EINVAL },
		{ "random text", false, EINVAL },
		{ 0, 0, 0}
	};
	
	for ( int i = 0; cases[i].text; ++i ) {
		INFO( "Case:" << i );

		bool got = false;
		int err = parse_bool( &got, cases[i].text );
		CHECK( got == cases[i].expected );
		CHECK( err == cases[i].err );
	}
}
