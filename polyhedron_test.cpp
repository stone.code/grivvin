// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include <lua.hpp>
#include <cstring>
#include <memory>

#include "vendor/catch.hpp"

extern lua_State* init_lua_state();
extern int exec_lua_script( lua_State* L, char const* source );

TEST_CASE( "polyhedron" )
{
	SECTION( "block" ) {
		char const* script =
			"v = shp:block( 0, 0, 0, 1, 2, 3)\n"
			"ret = not v:isempty()\n"
			"t1 = v:locate(0.5,1,1.5)\n"
			"t2 = v:locate(-1,2,0)\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		if (err!=0) {
			INFO( lua_tostring(L,-1) );
			lua_pop(L,1);
		REQUIRE( err == 0 );
		}
		REQUIRE( err == 0 );

		lua_getglobal(L, "ret");
		CHECK( lua_isboolean(L,-1) );
		CHECK( lua_toboolean(L,-1) );
		lua_getglobal(L, "t1");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
		lua_getglobal(L, "t2");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
	}

	SECTION( "gc" ) {
		char const* script =
			"v = shp:block(0,0,0,1,1,1)\n"
			"v = nil\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		lua_gc( L, LUA_GCCOLLECT, 0 );
	}
}
