Grivvin
=============

Grivvin is a scripted parametric 3D modelling tool, based on [Lua][Lua] and [CGAL][CGAL].
However, grivvin does not provide a GUI.  All models are specified by code.  Grivvin
can be used to generate complex 2D and 3D geometries.  Geometries are created primarily 
using CSG.  These shapes can then be exported to [DXF][DXF], postscript, or [STL][STL]
for manufacturing.

![Logo](https://bitbucket.org/rj/grivvin/downloads/logo.png)

Usage
-----

```
grivvin [-p POINTS] [FILENAME1] [[-p POINTS] [FILENAME2]]...
grivvin -h
grivvin --help
grivvin -v
grivvin --version
```

The arguments to grivvin are [Lua][Lua] scripts, which describe how the gemoetry
should be contructed.  The scripts are standard Lua scripts, so users should start
there.  Documentation for the functions provided by grivvin can be found in the
[Wiki][Wiki].

Using grivvin with -h or -v can be used to request help or version information,
respectively.

Download
--------

Binaries are not provided for this tool.  Please build from source.

Build
-----

If the dependencies are available, building grivvin should be as simple as:

```
configure
make
make install
```

The dependencies for grivvin are [Lua][Lua] and [CGAL][CGAL].

Note: The program has only been built locally on the development machine.  Please
let me know if there are any bugs in the build scripts.

License 
-------

Use of this source code is governed by a license, which is specified in the
[LICENSE](https://bitbucket.org/rj/grivvin/raw/default/LICENSE) file.

Contact
-------

To contact the tool maintainer, please send messages sent through BitBucket.
Pull requests or patches are welcome.

References
----------

* [The Computational Geometry Algorithms Library (CGAL)][CGAL]
* [DXF file format][DXF]
* [The Lua Programming Language][Lua]
* [STL file format][STL]

[CGAL]: http://www.cgal.org/ "The Computational Geometry Algorithms Library"
[DXF]: https://en.wikipedia.org/wiki/AutoCAD_DXF "AutoCAD DXF"
[Lua]: https://www.lua.org/ "The Lua Programming Language"
[STL]: https://en.wikipedia.org/wiki/STL_(file_format)  "STL (file format)"
[Wiki]: https://bitbucket.org/rj/grivvin/wiki/ "Wiki for grivvin"