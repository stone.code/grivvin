// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "triangulate_face.h"
#include <assert.h>
#include <utility>

using namespace grivvin;
typedef face_triangulation triangulation;
typedef std::pair<triangulation::Vertex_handle,triangulation::Vertex_handle> vertex_pair;

static vertex_pair insert_vertices( 
	triangulation& tri, kernel::Plane_3 const & plane, 
	polyhedron::SHalfedge_const_handle he )
{
	polyhedron::SHalfedge_const_handle lp = he;

	// Insert the first vertex
	polyhedron::Vertex_const_handle nef_v = lp->source()->center_vertex();
	triangulation::Vertex_handle tri_v = tri.insert( plane.to_2d( nef_v->point() ) );
	tri_v->info() = nef_v;
	triangulation::Vertex_handle first_tri_v = tri_v;

	// insert other vertices, along with constrained edges
	lp = lp->next();
	do {
		// insert the next vertex
		nef_v = lp->source()->center_vertex();
		triangulation::Vertex_handle new_tri_v = tri.insert( plane.to_2d( nef_v->point() ) );
		new_tri_v->info() = nef_v;
		
		// make the constrained edge
		tri.insert_constraint( tri_v, new_tri_v );
		
		// move to next vertex
		lp = lp->next();
		tri_v = new_tri_v;
	} while( lp != he );
	
	// Add final contrained edge
	tri.insert_constraint( tri_v, first_tri_v );
	
	// return last edge as marker for the cycle
	return std::make_pair( first_tri_v, tri_v );
}

static void fill_region( triangulation::Face_handle face )
{
	assert( !face->info() );
	face->info() = true;
	for ( int i = 0; i<3; ++i )
		if ( !face->is_constrained(i) && !face->neighbor(i)->info() ) fill_region( face->neighbor(i) );
}

static void insert_facets( triangulation& tri, vertex_pair vp )
{
	// first, clear out all faces
	for ( triangulation::All_faces_iterator face = tri.all_faces_begin();
		face != tri.all_faces_end(); ++face ) {
		face->info() = false;
	}

	// locate a starting face
	triangulation::Edge_circulator edge = tri.incident_edges( vp.first );
	do {
		int i = (edge->second + 1) % 3;
		int j = (edge->second + 2) % 3;
		assert( edge->first->vertex(j) == vp.first );
		if ( edge->first->vertex(i) == vp.second ) break;
		++edge;
	} while (true);
	
	// Set the flag
	assert( !tri.is_infinite( edge->first ) );
	assert( !edge->first->info() );
	fill_region( edge->first );
}

void grivvin::construct_face_triangulation( face_triangulation& tri, polyhedron::Halffacet_const_handle hf )
{
	// Make sure that the triangulation is clear
	tri.clear();

	// This plane is used to map 3D vertices to 2D
	kernel::Plane_3 plane = hf->plane();
	
	// Add the outside edge to the triangulation
	assert( hf->facet_cycles_begin() != hf->facet_cycles_end() );
	vertex_pair vp = insert_vertices( tri, plane, hf->facet_cycles_begin() );

	// Add the inside edges to the triangulation
	for ( polyhedron::Halffacet_cycle_const_iterator lp = ++hf->facet_cycles_begin();
		lp != hf->facet_cycles_end(); ++lp )
	{
		insert_vertices( tri, plane, lp );
	}
	
	// Mark the inside faces
	insert_facets( tri, vp );
}
