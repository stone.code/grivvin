// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "grivvin.h"
#include <assert.h>
#include <math.h>
#include <CGAL/convex_hull_3.h>

using grivvin::polyhedron;
using grivvin::field;
using grivvin::point3d;

polyhedron grivvin::create_sphere( field radius, point3d center, unsigned points )
{
	assert( radius > 0 );

	std::vector<point3d> pts;
	
	for ( unsigned lp=0; lp<=points; ++lp ) {
		double theta = lp * M_PI_2 / (points+1);
		double c1 = CGAL::to_double(radius) * cos( theta );
		double s1 = CGAL::to_double(radius) * sin( theta );

		pts.push_back( point3d( center.x() + c1, center.y() + s1, center.z() ) );
		pts.push_back( point3d( center.x() - s1, center.y() + c1, center.z() ) );
		pts.push_back( point3d( center.x() - c1, center.y() - s1, center.z() ) );
		pts.push_back( point3d( center.x() + s1, center.y() - c1, center.z() ) );
	}

	for ( unsigned lp2=1; lp2<=points; ++lp2 ) {
		double phi = lp2 * M_PI_2 / (points+1);
		double c2 = cos( phi );
		double s2 = CGAL::to_double(radius) * sin( phi );
		
		for ( unsigned lp=0; lp<=points-lp2; ++lp ) {
			double theta = lp * M_PI_2 / (points+1-lp2);
			double c1 = CGAL::to_double(radius) * cos( theta );
			double s1 = CGAL::to_double(radius) * sin( theta );
			
			pts.push_back( point3d( center.x() + c1*c2, center.y() + s1*c2, center.z() + s2 ) );
			pts.push_back( point3d( center.x() - s1*c2, center.y() + c1*c2, center.z() + s2 ) );
			pts.push_back( point3d( center.x() - c1*c2, center.y() - s1*c2, center.z() + s2 ) );
			pts.push_back( point3d( center.x() + s1*c2, center.y() - c1*c2, center.z() + s2 ) );
			pts.push_back( point3d( center.x() + c1*c2, center.y() + s1*c2, center.z() - s2 ) );
			pts.push_back( point3d( center.x() - s1*c2, center.y() + c1*c2, center.z() - s2 ) );
			pts.push_back( point3d( center.x() - c1*c2, center.y() - s1*c2, center.z() - s2 ) );
			pts.push_back( point3d( center.x() + s1*c2, center.y() - c1*c2, center.z() - s2 ) );
		}
	}
	pts.push_back( point3d( center.x(), center.y(), center.z() + radius ) );
	pts.push_back( point3d( center.x(), center.y(), center.z() - radius ) );

	// Create the polyhedron
	CGAL::Polyhedron_3<grivvin::kernel> block;
	CGAL::convex_hull_3( pts.begin(), pts.end(), block );
	assert( !block.empty() );
	assert( block.is_closed() );
	return block;
}

static int largest_dim( grivvin::vector3d v ) {
	field x = abs(v.x());
	field y = abs(v.y());
	
	if ( x > y ) {
		return x > abs(v.z()) ? 1 : 3;
	}
	else {
		return y > abs(v.z()) ? 2 : 3;
	}
}

static grivvin::vector3d proj(grivvin::vector3d u, grivvin::vector3d v )
{
	return (u*v) / (u*u) * u;
}

polyhedron	grivvin::create_cylinder( field radius, point3d a, point3d b, unsigned points )
{
	vector3d dz = b - a;
	vector3d dx, dy;
	
	switch ( largest_dim(dz) ) {
		case 1:	dx=vector3d(0,1,0); dy=vector3d(0,0,1); break;
		case 2:	dx=vector3d(0,0,1); dy=vector3d(1,0,0); break;
		case 3:	dx=vector3d(1,0,0); dy=vector3d(0,1,0); break;
	}
	
	dx = dx - proj(dz,dx);
	dy = dy - proj(dz,dy);
	dy = dy - proj(dx,dy); // TODO:  necessary?
	
	dx = dx / sqrt( CGAL::to_double( dx.squared_length() ) );
	dy = dy / sqrt( CGAL::to_double( dy.squared_length() ) );
	
	workplane w1, w2;
	w1.origin = a;
	w1.dx = dx;
	w1.dy = dy;
	w2.origin = b;
	w2.dx = dx;
	w2.dy = dy;
	
	return extrude( create_circle( radius, 0, 0, points ), w1, w2 );
}
