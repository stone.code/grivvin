-- Start
-- Script: script.lua
print("Creating the part")
print("-- plane count", get_point_count() )
print("-- base name", basename() )
inch = 25.4
foot = 12 * inch

heights = {}
heights[0] = 6 * inch
heights[1] = 14 * inch
heights[2] = 14 * inch
heights[3] = 12 * inch
heights[4] = 12 * inch
heights[5] = 10 * inch
heights[6] = 10 * inch
heights[7] = 10 * inch

bt = inch * 3 / 4
tbt = inch * 11/16
cable_radius = 0.25*inch + inch/64
tool_radius = 0.25*inch + inch/64

total_height = 0
for i = 0 ,7 do
	total_height = total_height + heights[i] + bt
	print( total_height / inch )
end
print( "Height = ", total_height/inch, total_height / foot )

piece = pgn:rect( 0, 0, 12*inch, total_height )
piece:write_ps( basename() .. ".ps", 0.001  )

width1 = 11 * inch
width2 = 9 * inch

x0 = (width2^2 + total_height^2 - width1^2)/2/(width1-width2)
radius = x0 + width1
theta = math.asin( total_height/radius )

set_point_count(101)
piece = piece * pgn:circle( radius, -x0, 0 )
set_point_count(9)
-- base board
piece = piece - pgn:rect( 0, 0, 0.5*inch, 3*inch )

pos = 0
for i = 0,7 do
	pos = pos + heights[i] + bt
	piece = piece - pgn:rect(-inch,pos-tbt,1.5*inch,pos)
	piece = piece - pgn:circle(tool_radius,1.5*inch-tool_radius,pos)
	piece = piece - pgn:circle(tool_radius,1.5*inch-tool_radius,pos-tbt)
	tmp = math.asin( pos / radius )
	tmp = -x0 + radius * math.cos(tmp)
	piece = piece - pgn:rect(tmp-1.5*inch,pos-tbt,tmp+inch,pos)
	piece = piece - pgn:circle(tool_radius,tmp-1.5*inch+tool_radius,pos)
	piece = piece - pgn:circle(tool_radius,tmp-1.5*inch+tool_radius,pos-tbt)
end

function board(x,y,width)
	b = pgn:rect(x,y,x+width,y+24*inch)
	b = b - pgn:rect( x+1.5*inch,y,x+width-1.5*inch, y+tbt )
	b = b - pgn:rect( x+1.5*inch,y+24*inch-tbt, x+width-1.5*inch,y+24*inch)
	b = b - pgn:circle( tool_radius, x+1.5*inch+tool_radius,y+tbt )
	b = b - pgn:circle( tool_radius, x+width-1.5*inch-tool_radius,y+tbt )
	b = b - pgn:circle( tool_radius, x+1.5*inch+tool_radius,y+24*inch-tbt )
	b = b - pgn:circle( tool_radius, x+width-1.5*inch-tool_radius,y+24*inch-tbt )
	b = b - pgn:circle( cable_radius, x, y+2*bt+cable_radius )
	b = b - pgn:circle( cable_radius, x, y+24*inch-(2*bt+cable_radius) )
	b = b - pgn:circle( cable_radius, x, y+12*inch )
	return b
end

pos = 0
for i = 0,7,2 do
	pos = pos + heights[i] + bt
	tmp = math.asin( pos / radius )
	tmp = -x0 + radius * math.cos(tmp)
	print( "Board", i+1, tmp/inch )
	piece = piece + board((i+2)*0.75*foot, 0, tmp)
	pos = pos + heights[i+1] + bt
	tmp = math.asin( pos / radius )
	tmp = -x0 + radius * math.cos(tmp)
	print( "Board", i+2, tmp/inch )
	piece = piece + board((i+2)*0.75*foot, 3*foot, tmp)
end

piece:write_ps( basename() .. ".ps", 0.001  )
