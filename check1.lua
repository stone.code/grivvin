-- Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
-- Use of this source code is governed the license found in the LICENSE file.

size = 100
height = 50

wpbase = workplane( point3d(0,0,0), vector3d(1,0,0), vector3d(0,1,0) );

pgn1 = pgn:rect( -size*0.5, -size*0.5, size*0.5, size*0.5 )
pgn2 = pgn:rect( -size*0.25, -size*0.25, size*0.25, size*0.25 )

shape = wpbase:extrude( pgn1, vector3d(0,0,height) )
shape:extrude( wpbase:translate(0,0,height), pgn2, vector3d(0,0,height) )

-- Write
print("Writing output.")
shape:write_stl("check1.stl")
