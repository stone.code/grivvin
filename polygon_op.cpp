// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "grivvin.h"
#include <assert.h>

using grivvin::polygon;

polygon	grivvin::operator+( polygon const& a, polygon const& b )
{
	//assert( a.is_valid() );
	//assert( b.is_valid() );

	polygon p;
	p.join( a, b );
	return p;
}

polygon grivvin::operator-( polygon const& a, polygon const& b )
{
	//assert( a.is_valid() );
	//assert( b.is_valid() );

	polygon p;
	p.difference( a, b );
	return p;
}

polygon grivvin::operator*( polygon const& a, polygon const& b )
{
	//assert( a.is_valid() );
	//assert( b.is_valid() );

	polygon p;
	p.intersection( a, b );
	return p;
}

polygon grivvin::operator^( polygon const& a, polygon const& b )
{
	//assert( a.is_valid() );
	//assert( b.is_valid() );

	polygon p;
	p.symmetric_difference( a, b );
	return p;
}
