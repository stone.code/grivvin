-- Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
-- Use of this source code is governed the license found in the LICENSE file.

set_point_count(3)

s = shp:sphere( 10, point3d(0,0,0) )
shape = s + s:scale(0.5):translate(0,0,10)

-- Write
print("Writing output.")
shape:write_stl("check3.stl")
