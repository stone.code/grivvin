package main

import (
	"github.com/gonum/plot/vg"
	"github.com/gonum/plot/vg/vgimg"
	"image/color"
	"os"
)

func must(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func makeRect(x1, y1, x2, y2 vg.Length) vg.Path {
	var ret vg.Path

	ret.Move(vg.Point{x1, y1})
	ret.Line(vg.Point{x2, y1})
	ret.Line(vg.Point{x2, y2})
	ret.Line(vg.Point{x1, y2})
	ret.Close()
	return ret
}

func makeLine(x1, y1, x2, y2 vg.Length) vg.Path {
	var ret vg.Path

	ret.Move(vg.Point{x1, y1})
	ret.Line(vg.Point{x2, y2})
	ret.Close()
	return ret
}

const R = 96

func main() {
	const (
		x1 = 2
		x2 = x1 + R/8
		x4 = R - 2
		x3 = x4 - R/8
		y1 = R/4 + 4
		y2 = y1 + R/8
		y3 = y2 + R/8
	)

	font, err := vg.MakeFont("Helvetica", 33.3)
	must(err)
	canvas := vgimg.NewWith(vgimg.UseWH(R, y3+2), vgimg.UseDPI(R))
	// Write the name
	canvas.FillString(font, vg.Point{0.25, 8}, "grivvin")
	println(font.Width("grivvin"))
	// remove the points on the 'i's
	canvas.SetColor(color.Gray{0xff})
	canvas.Fill(makeRect(1, 33, R, 36))
	canvas.SetColor(color.RGBA{0x40, 0xC0, 0x40, 0xff})
	canvas.Stroke(makeLine(x1, y1, x1, y3))
	canvas.Stroke(makeLine(x4, y1, x4, y3))
	canvas.Stroke(makeLine(x1, y2, x4, y2))
	canvas.Stroke(makeLine(x1, y2, x2, y3))
	canvas.Stroke(makeLine(x1, y2, x2, y1))
	canvas.Stroke(makeLine(x4, y2, x3, y3))
	canvas.Stroke(makeLine(x4, y2, x3, y1))

	file, err := os.Create("./logo.png")
	must(err)
	defer file.Close()

	_, err = vgimg.PngCanvas{canvas}.WriteTo(file)
	must(err)
}
