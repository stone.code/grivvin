// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#include "luag.h"

void luaG_addfunction( lua_State *L, char const* name, lua_CFunction fn )
{
	lua_pushstring( L, name );
	lua_pushcfunction( L, fn );
	lua_settable( L, -3 );
}

void luaG_checkargcount( lua_State *L, int c )
{
	if (lua_gettop(L)!=c) {
		luaL_error(L,"incorrect number of arguments");
	}
}

int  luaG_checkutype( lua_State *L, int ud, char const* tname )
{
	if (lua_getmetatable(L, ud)) {  /* does it have a metatable? */
		lua_getfield(L, LUA_REGISTRYINDEX, tname);  /* get correct metatable */
		if (lua_rawequal(L, -1, -2)) {  /* does it have the correct mt? */
			lua_pop(L, 2);  /* remove both metatables */
			return -1;
		}
		lua_pop(L,2);
	}
	
	return 0;
}
