// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#include "grivvin.h"
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/Polyhedron_3.h>
#include <assert.h>
#include <iterator>

using namespace grivvin;

typedef CGAL::Polyhedron_3<kernel> Polyhedron_3;
typedef Polyhedron_3::Halfedge_handle Halfedge_handle;

static void construct_polyhedron( Polyhedron_3& p, std::vector<point3d> const& pts )
{
	assert( pts.size() >= 6 );
	assert( pts.size() % 2 == 0 );

	// we now need to make a polyhedron
	Halfedge_handle h = p.make_tetrahedron( pts[2], pts[1], pts[0], pts[4] );
	assert( h->vertex()->point() == pts[2] );
	Halfedge_handle g = h->next()->opposite()->next();
	assert( g->vertex()->point() == pts[4] );
	p.split_edge( h->next() );
	p.split_edge( g->next() );
	h->next()->vertex()->point() = pts[3];
	g->next()->vertex()->point() = pts[5];
	p.split_facet( g->next(), g->next()->next()->next() );
	assert( p.is_valid() );
	assert( p.is_closed() );
	
	for ( size_t v = 6; v < pts.size(); v+=2 ) {
		// add next vertex
		assert( g->vertex()->point() == pts[ v-2 ] );
		assert( g->next()->vertex()->point() == pts[ v-1 ] );
		g = g->next()->opposite()->next();
		assert( g->vertex()->point() == pts[0] );
		assert( g->next()->vertex()->point() == pts[1] );
		
		// split edges to add next point
		Halfedge_handle vertex1 = p.split_edge( g );
		vertex1->vertex()->point() = pts[v];
		assert( vertex1->next() == g );
		Halfedge_handle vertex2 = p.split_edge( g->next()->next() );
		vertex2->vertex()->point() = pts[v+1];
		assert( vertex2 == g->next()->next() );
		p.split_facet( vertex1, vertex2 );
		
		assert( p.is_valid() );
		assert( p.is_closed() );
		
		// update position g
		g = vertex1;
	}
}

static polyhedron extrude( CGAL::Polygon_2<kernel> const& pgn, workplane const& wp1, workplane const& wp2 )
{
	assert( ! pgn.is_empty() );
	assert( wp1.dx.squared_length() > 0 );
	assert( wp1.dy.squared_length() > 0 );
	assert( wp2.dx.squared_length() > 0 );
	assert( wp2.dy.squared_length() > 0 );

	std::vector<point3d> pts;
	for ( CGAL::Polygon_2<kernel>::Vertex_iterator v = pgn.vertices_begin(); v != pgn.vertices_end(); ++v ) {
		point2d a = *v;
		pts.push_back( wp1.origin + wp1.dx*a.x() + wp1.dy*a.y() );
		pts.push_back( wp2.origin + wp2.dx*a.x() + wp2.dy*a.y() );
	}
	
	Polyhedron_3 p;
	construct_polyhedron( p, pts );
	
	// create the return polyhedron
	polyhedron ret(p);
	assert( !ret.is_empty() );
	assert( !ret.is_space() );
	assert( ret == ret.regularization() );
	return ret;
}

polyhedron grivvin::extrude( polygon pgn, workplane const& wp, vector3d dz )
{
	assert( ! pgn.is_empty() );
	assert( ! pgn.is_plane() );
	assert( wp.dx.squared_length() > 0 );
	assert( wp.dy.squared_length() > 0 );
	assert( dz.squared_length() > 0 );
	
	return extrude( pgn, wp, wp.translate(dz) );
}

polyhedron	grivvin::extrude( polygon pgn, workplane const& wp1, workplane const& wp2 )
{
	assert( ! pgn.is_empty() );
	assert( ! pgn.is_plane() );
	assert( wp1.dx.squared_length() > 0 );
	assert( wp1.dy.squared_length() > 0 );
	assert( wp2.dx.squared_length() > 0 );
	assert( wp2.dy.squared_length() > 0 );

	std::vector<CGAL::Polygon_with_holes_2<kernel> > pwhs;
	pgn.polygons_with_holes( std::back_inserter( pwhs ) );

	polyhedron ret( polyhedron::EMPTY );
	for ( std::vector<CGAL::Polygon_with_holes_2<kernel> >::const_iterator pwh = pwhs.begin();
		pwh != pwhs.end(); ++pwh ) {

		// Create volume from outer boundary
		polyhedron tmp = ::extrude( pwh->outer_boundary(), wp1, wp2 );

		// Substract volumes for holes
		for ( CGAL::Polygon_with_holes_2<kernel>::Hole_const_iterator h = pwh->holes_begin();
			h != pwh->holes_end(); ++h ) {
			tmp -= ::extrude( *h, wp1, wp2 );
		}

		// Add region to the total
		ret += tmp.closure();
	}

	assert( !ret.is_empty() );
	assert( !ret.is_space() );
	assert( ret == ret.regularization() );
	return ret;
}
