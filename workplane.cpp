// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include <lua.hpp>
#include "luag.h"
#include "grivvin.h"

using grivvin::point3d;
using grivvin::polygon;
using grivvin::vector3d;
using grivvin::workplane;
using grivvin::polyhedron;

extern unsigned curve_point_count;

template <>
char const* const luaG_traits<workplane>::metatable = "grivvin.workplane";

static int wp_new_1(lua_State *L) 
{
	luaL_argcheck(L,lua_isstring(L,1),1,"expected string");
	
	workplane ret;
	
	char const* name = lua_tostring(L,1);
	if (strcmp(name,"xy")==0) {
		ret.origin = point3d(0,0,0);
		ret.dx = vector3d(1,0,0);
		ret.dy = vector3d(0,1,0);
	} else if (strcmp(name,"yz")==0) {
		ret.origin = point3d(0,0,0);
		ret.dx = vector3d(0,1,0);
		ret.dy = vector3d(0,0,1);
	} else if (strcmp(name,"zx")==0) {
		ret.origin = point3d(0,0,0);
		ret.dx = vector3d(0,0,1);
		ret.dy = vector3d(1,0,0);
	} else {
		return luaL_error(L,"name for plane was not recognized");
	}
	
	luaG_push( L, ret );
	return 1;
}
	
static int wp_new_3(lua_State *L) 
{
	point3d* origin = luaG_checkudata<point3d>(L,1);
	vector3d* dx = luaG_checkudata<vector3d>(L,2);
	vector3d* dy = luaG_checkudata<vector3d>(L,3);
	
	workplane ret;
	ret.origin = *origin;
	ret.dx = *dx;
	ret.dy = *dy;

	luaG_push( L, ret );
	return 1;
}

static int wp_new(lua_State *L) 
{
	switch (lua_gettop(L)) {
		case 1:
			return wp_new_1(L);
		case 3:
			return wp_new_3(L);
	}
	
	return luaL_error(L,"incorrect number of arguments");
}

static int wp_gc(lua_State *L) 
{
	workplane* ud = luaG_checkudata<workplane>(L,1);
	reinterpret_cast<workplane*>(ud)->~workplane();
	return 0;
}

static int wp_tostring(lua_State *L)
{
	luaG_checkargcount( L, 1 );
	workplane* ud = luaG_checkudata<workplane>(L,1);
	char buffer[ 2+ (1+2+10+1+2)*3*3 + 6 + 6 + 2 + 1];
	
	int n = snprintf( buffer, sizeof(buffer), "((%g,%g,%g),(%g,%g,%g),(%g,%g,%g))", 
		CGAL::to_double(ud->origin.x()), CGAL::to_double(ud->origin.y()), CGAL::to_double(ud->origin.z()),
		CGAL::to_double(ud->dx.x()), CGAL::to_double(ud->dx.y()), CGAL::to_double(ud->dx.z()),
		CGAL::to_double(ud->dy.x()), CGAL::to_double(ud->dy.y()), CGAL::to_double(ud->dy.z())
	);
	assert( n>=0 && static_cast<size_t>(n)<sizeof(buffer) );
	lua_pushstring(L,buffer);
	return 1;
}

static int wp_extrude(lua_State *L) 
{
	luaG_checkargcount(L,3);
	workplane* wp = luaG_checkudata<workplane>(L,1);
	polygon* pgn = luaG_checkudata<polygon>(L,2);
	
	if ( luaG_checkutype(L,3,"grivvin.workplane") ) {
		workplane* dz = luaG_checkudata<workplane>(L,3);
		polyhedron ret = grivvin::extrude( *pgn,
			*wp,*dz );
	
		luaG_push( L, ret );
		return 1;
	}
	
	vector3d* dz = luaG_checkudata<vector3d>(L,3);
	polyhedron ret = grivvin::extrude( *pgn,
		*wp,
		*dz );
	luaG_push( L, ret );
	return 1;
}

static int wp_origin(lua_State *L) 
{
	luaG_checkargcount(L,1);
	workplane* wp = luaG_checkudata<workplane>(L,1);
	luaG_push( L, wp->origin );
	return 1;
}

static int wp_revolve(lua_State *L) 
{
	luaG_checkargcount(L,2);
	workplane* ud1 = luaG_checkudata<workplane>(L,1);
	polygon* ud2 = luaG_checkudata<polygon>(L,2);
	
	polyhedron ret = grivvin::revolve( *ud2, *ud1, curve_point_count );
	luaG_push( L, ret );
	return 1;
}

static int wp_translate_2(lua_State *L) 
{
	workplane* wp = luaG_checkudata<workplane>(L,1);
	vector3d* dz = luaG_checkudata<vector3d>(L,2);
	
	workplane ret = wp->translate( *dz );
	luaG_push( L, ret );
	return 1;
}

static int wp_translate_4(lua_State *L) 
{
	workplane* wp = luaG_checkudata<workplane>(L,1);
	luaL_argcheck(L,lua_isnumber(L,2), 2, "number expected" );
	luaL_argcheck(L,lua_isnumber(L,3), 3, "number expected" );
	luaL_argcheck(L,lua_isnumber(L,4), 4, "number expected" );
	
	lua_Number dx = lua_tonumber(L,2);
	lua_Number dy = lua_tonumber(L,3);
	lua_Number dz = lua_tonumber(L,4);
	
	luaG_push( L, wp->translate( vector3d(dx,dy,dz) ) );
	return 1;
}

static int wp_translate(lua_State *L) 
{
	switch( lua_gettop(L) ) {
		case 2:
			return wp_translate_2(L);
		case 4:
			return wp_translate_4(L);
	}
	return luaL_error(L,"incorrect number of arguments");
}

static int wp_scale(lua_State *L) 
{
	if (lua_gettop(L)!=2) {
		return luaL_error(L,"incorrect number of arguments");
	}

	workplane* wp = luaG_checkudata<workplane>(L,1);
	lua_Number s = luaL_checknumber( L, 2 );
	
	luaG_push( L, wp->scale( s ) );
	return 1;
}

void wp_register( lua_State *L )
{
	luaL_newmetatable(L,luaG_traits<workplane>::metatable);
	lua_pushstring(L, "__index");
	lua_pushvalue(L,-2); /* pushes the metatable */
	lua_settable(L,-3); /*metatable.__index = metatable */
	luaG_addfunction(L, "__gc", wp_gc );
	luaG_addfunction(L, "__tostring", wp_tostring );
	luaG_addfunction(L, "extrude",wp_extrude);
	luaG_addfunction(L, "origin", wp_origin );
	luaG_addfunction(L, "revolve",wp_revolve);
	luaG_addfunction(L, "translate",wp_translate);
	luaG_addfunction(L, "scale",wp_scale);
	
	lua_register( L, "workplane", wp_new );
}
