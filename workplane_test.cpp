// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include <lua.hpp>
#include <cstring>
#include <memory>

#include "vendor/catch.hpp"

extern lua_State* init_lua_state();
extern int exec_lua_script( lua_State* L, char const* source );

TEST_CASE( "workplane" )
{
	SECTION( "new" ) {
		char const* script =
			"v = workplane(\"xy\")\n"
			"ret = v:origin():iszero()\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "ret");
		CHECK( lua_isboolean(L,-1) );
		CHECK( lua_toboolean(L,-1) );
	}

	SECTION( "new" ) {
		char const* script =
			"ret1 = tostring(workplane(\"xy\"))\n"
			"ret2 = tostring(workplane(\"yz\"))\n"
			"ret3 = tostring(workplane(\"zx\"))\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "ret1");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string( "((0,0,0),(1,0,0),(0,1,0))") );
		lua_getglobal(L, "ret2");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string( "((0,0,0),(0,1,0),(0,0,1))") );
		lua_getglobal(L, "ret3");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string( "((0,0,0),(0,0,1),(1,0,0))") );
	}

	SECTION( "new" ) {
		char const* script =
			"wp = workplane(point3d(1,2,3),vector3d(1,1,0),vector3d(0,1,1))\n"
			"ret = tostring(wp)\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "ret");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string( "((1,2,3),(1,1,0),(0,1,1))") );
	}

	SECTION( "gc" ) {
		char const* script =
			"v = workplane(\"xy\")\n"
			"v = nil\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		lua_gc( L, LUA_GCCOLLECT, 0 );
	}

	SECTION( "translate" ) {
		char const* script =
			"wp = workplane(\"xy\"):scale(2.3)\n"
			"ret = tostring(wp)\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "ret");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string( "((0,0,0),(2.3,0,0),(0,2.3,0))") );
	}

	SECTION( "translate" ) {
		char const* script =
			"wp = workplane(\"xy\"):translate(vector3d(1.2,3.4,5.6))\n"
			"ret = tostring(wp)\n";
			
		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		
		lua_getglobal(L, "ret");
		CHECK( lua_isstring(L,-1) );
		CHECK( lua_tostring(L,-1) == std::string( "((1.2,3.4,5.6),(1,0,0),(0,1,0))") );
	}
}
