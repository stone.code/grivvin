// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "grivvin.h"
#include "luag.h"
#include <lua.hpp>

using grivvin::point3d;
using grivvin::polygon;
using grivvin::vector3d;
using grivvin::polyhedron;
using grivvin::workplane;

extern unsigned curve_point_count;

template <>
char const* const luaG_traits<polyhedron>::metatable = "grivvin.polyhedron";

static int shp_block( lua_State* L )
{
    luaG_checkargcount( L, 7 );
    lua_Number x1 = luaL_checknumber( L, 2 );
    lua_Number y1 = luaL_checknumber( L, 3 );
    lua_Number z1 = luaL_checknumber( L, 4 );
    lua_Number x2 = luaL_checknumber( L, 5 );
    lua_Number y2 = luaL_checknumber( L, 6 );
    lua_Number z2 = luaL_checknumber( L, 7 );

    grivvin::polygon pgn = grivvin::create_rect( x1, y1, x2, y2 );
    grivvin::workplane a, b;
    a.origin = grivvin::point3d( 0, 0, z1 );
    a.dx = grivvin::vector3d( 1, 0, 0 );
    a.dy = grivvin::vector3d( 0, 1, 0 );
    b.origin = grivvin::point3d( 0, 0, z2 );
    b.dx = grivvin::vector3d( 1, 0, 0 );
    b.dy = grivvin::vector3d( 0, 1, 0 );

    polyhedron ret = grivvin::extrude( pgn, a, b );
    luaG_push( L, ret );
    return 1;
}

static int shp_sphere_3( lua_State* L )
{
    lua_Number radius = luaL_checknumber( L, 2 );
    point3d* origin = luaG_checkudata<point3d>( L, 3 );

    polyhedron ret =
        grivvin::create_sphere( radius, *origin, curve_point_count );
    luaG_push( L, ret );
    return 1;
}

static int shp_sphere_5( lua_State* L )
{
    lua_Number radius = luaL_checknumber( L, 2 );
    lua_Number x = luaL_checknumber( L, 3 );
    lua_Number y = luaL_checknumber( L, 4 );
    lua_Number z = luaL_checknumber( L, 5 );

    polyhedron ret =
        grivvin::create_sphere( radius, point3d( x, y, z ), curve_point_count );
    luaG_push( L, ret );

    return 1;
}

static int shp_sphere( lua_State* L )
{
    switch ( lua_gettop( L ) ) {
    case 3:
        return shp_sphere_3( L );
    case 5:
        return shp_sphere_5( L );
    }

    return luaL_error( L, "incorrect number of arguments" );
}

static int shp_cylinder( lua_State* L )
{
    luaG_checkargcount( L, 4 );
    lua_Number radius = luaL_checknumber( L, 2 );
    point3d* a = luaG_checkudata<point3d>( L, 3 );
    point3d* b = luaG_checkudata<point3d>( L, 4 );

    luaG_push( L,
               grivvin::create_cylinder( radius, *a, *b, curve_point_count ) );
    return 1;
}

static int shp_gc( lua_State* L )
{
    polyhedron* ud = luaG_checkudata<polyhedron>( L, 1 );
    reinterpret_cast<polyhedron*>( ud )->~polyhedron();
    return 0;
}

static int shp_add( lua_State* L )
{
    polyhedron* ud1 = luaG_checkudata<polyhedron>( L, 1 );
    polyhedron* ud2 = luaG_checkudata<polyhedron>( L, 2 );

    polyhedron ret = *ud1 + *ud2;
    assert( ret == ret.regularization() );
    luaG_push( L, ret );
    return 1;
}

static int shp_sub( lua_State* L )
{
    polyhedron* ud1 = luaG_checkudata<polyhedron>( L, 1 );
    polyhedron* ud2 = luaG_checkudata<polyhedron>( L, 2 );

    polyhedron ret = ( *ud1 - *ud2 ).regularization();
    luaG_push( L, ret );
    return 1;
}

static int shp_mul( lua_State* L )
{
    polyhedron* ud1 = luaG_checkudata<polyhedron>( L, 1 );
    polyhedron* ud2 = luaG_checkudata<polyhedron>( L, 2 );

    polyhedron ret = *ud1 * *ud2;
    assert( ret == ret.regularization() );
    luaG_push( L, ret );
    return 1;
}

static int shp_complement( lua_State* L )
{
	luaG_checkargcount( L, 1 );
    polyhedron* ud1 = luaG_checkudata<polyhedron>( L, 1 );

    luaG_push( L, ud1->complement().regularization() );
    return 1;
}

static int shp_extrude( lua_State* L )
{
    luaG_checkargcount( L, 4 );
    polyhedron* ud1 = luaG_checkudata<polyhedron>( L, 1 );
    workplane* ud2 = luaG_checkudata<workplane>( L, 2 );
    polygon* ud3 = luaG_checkudata<polygon>( L, 3 );

    if ( luaG_checkutype( L, 4, "grivvin.workplane" ) ) {
        workplane* ud4 = luaG_checkudata<workplane>( L, 4 );
        polyhedron tmp =
            extrude( *ud3,
                     *ud2,
                     *ud4 );

        *ud1 += tmp;
        return 0;
    }

    vector3d* ud4 = luaG_checkudata<vector3d>( L, 4 );
    polyhedron tmp = extrude( *ud3, *ud2, *ud4 );
    *ud1 += tmp;
    return 0;
}

static int shp_cut( lua_State* L )
{
    luaG_checkargcount( L, 4 );
    polyhedron* ud1 = luaG_checkudata<polyhedron>( L, 1 );
    workplane* ud2 = luaG_checkudata<workplane>( L, 2 );
    polygon* ud3 = luaG_checkudata<polygon>( L, 3 );
    vector3d* ud4 = luaG_checkudata<vector3d>( L, 4 );

    polyhedron tmp = extrude( *ud3, *ud2, *ud4 );
    *ud1 = ( *ud1 - tmp ).regularization();
    return 0;
}

static int shp_write_stl( lua_State* L )
{
    luaG_checkargcount( L, 2 );
    polyhedron* ud = luaG_checkudata<polyhedron>( L, 1 );
    char const* filename = luaL_checkstring( L, 2 );

    grivvin::write_stl( filename, *ud );
    return 0;
}

static int shp_cross_section( lua_State* L )
{
    luaG_checkargcount( L, 5 );
    polyhedron* ud = luaG_checkudata<polyhedron>( L, 1 );
    lua_Number a = luaL_checknumber( L, 2 );
    lua_Number b = luaL_checknumber( L, 3 );
    lua_Number c = luaL_checknumber( L, 4 );
    lua_Number d = luaL_checknumber( L, 5 );

    polyhedron tmp;
    tmp = ud->intersection(
        grivvin::plane3d( a, b, c, d ), polyhedron::CLOSED_HALFSPACE );
    luaG_push( L, tmp );
    return 1;
}

static int shp_translate_2( lua_State* L )
{
    polyhedron* u1 = luaG_checkudata<polyhedron>( L, 1 );
    vector3d* u2 = luaG_checkudata<vector3d>( L, 2 );

    grivvin::trans3d t( CGAL::TRANSLATION, *u2 );

	polyhedron ret( *u1 );
	ret.transform(t);
	luaG_push( L, ret );
    return 1;
}

static int shp_translate_4( lua_State* L )
{
    polyhedron* u1 = luaG_checkudata<polyhedron>( L, 1 );
    lua_Number x = luaL_checknumber( L, 2 );
    lua_Number y = luaL_checknumber( L, 3 );
    lua_Number z = luaL_checknumber( L, 4 );

    grivvin::trans3d t( CGAL::TRANSLATION, vector3d( x, y, z ) );

	polyhedron ret( *u1 );
	ret.transform(t);
	luaG_push( L, ret );
    return 1;
}

static int shp_translate( lua_State* L )
{
    switch ( lua_gettop( L ) ) {
    case 2:
        return shp_translate_2( L );
    case 4:
        return shp_translate_4( L );
    }

    return luaL_error( L, "incorrect number of arguments" );
}

static int shp_scale( lua_State* L )
{
    polyhedron* u1 = luaG_checkudata<polyhedron>( L, 1 );
    lua_Number s = luaL_checknumber( L, 2 );

    grivvin::trans3d t( CGAL::SCALING, s );

	polyhedron ret( *u1 );
	ret.transform(t);
	luaG_push( L, ret );
    return 1;
}

static int shp_wp( lua_State* L )
{
    luaG_checkargcount( L, 2 );
    polyhedron* ud = luaG_checkudata<polyhedron>( L, 1 );
    char const* name = luaL_checkstring( L, 2 );

    grivvin::workplane ret;

    if ( strcmp( name, "top" ) == 0 ) {
        polyhedron::Vertex_const_iterator lp = ud->vertices_begin();
        grivvin::field z = lp->point().z();

        for ( ++lp; lp != ud->vertices_end(); ++lp ) {
            if ( lp->point().z() > z )
                z = lp->point().z();
        }
        ret.origin = grivvin::point3d( 0, 0, z );
        ret.dx = grivvin::vector3d( 1, 0, 0 );
        ret.dy = grivvin::vector3d( 0, 1, 0 );
    } else {
        return luaL_error( L, "name for plane was not recognized" );
    }

	luaG_push( L, ret );
    return 1;
}

static int shp_isempty( lua_State* L )
{
	luaG_checkargcount(L,1);
	polyhedron* ud = luaG_checkudata<polyhedron>(L,1);
	bool const ret = ud->is_empty();
	lua_pushboolean(L,ret);
	return 1;
}

static int shp_locate(lua_State *L)
{
	luaG_checkargcount(L,4);
	polyhedron* ud = luaG_checkudata<polyhedron>(L,1);
	lua_Number x = luaL_checknumber(L,2);
	lua_Number y = luaL_checknumber(L,3);
	lua_Number z = luaL_checknumber(L,4);

	polyhedron::Object_handle ret = ud->locate( point3d(x,y,z) );
	polyhedron::Volume_const_handle v;
	if ( !CGAL::assign(v, ret) ) {
		// On the border between two volumes
		lua_pushnumber( L, 0 );
		return 1;
	}
	lua_pushnumber( L, v->mark() ? 1 : -1 );
	return 1;
}

static luaL_Reg shp_fns[] = {{"block", shp_block},
                             {"sphere", shp_sphere},
                             {"cylinder", shp_cylinder},
                             {0, 0}};

void shp_register( lua_State* L )
{
#if LUA_VERSION_NUM > 501
    lua_newtable( L );
    luaL_setfuncs( L, shp_fns, 0 );
    lua_pushvalue( L, -1 );    // pluck these lines out if they offend you
    lua_setglobal( L, "shp" ); // for they clobber the Holy _G
#else
    luaL_register( L, "shp", shp_fns );
#endif

    luaL_newmetatable( L, "grivvin.polyhedron" );
    lua_pushstring( L, "__index" );
    lua_pushvalue( L, -2 ); /* pushes the metatable */
    lua_settable( L, -3 );  /*metatable.__index = metatable */
    luaG_addfunction( L, "__gc", shp_gc );
    luaG_addfunction( L, "__add", shp_add );
    luaG_addfunction( L, "__sub", shp_sub );
    luaG_addfunction( L, "__mul", shp_mul );
    luaG_addfunction( L, "complement", shp_complement );
    luaG_addfunction( L, "cross_section", shp_cross_section );
    luaG_addfunction( L, "cut", shp_cut );
    luaG_addfunction( L, "extrude", shp_extrude );
    luaG_addfunction( L, "isempty", shp_isempty);
    luaG_addfunction( L, "locate", shp_locate);
    luaG_addfunction( L, "translate", shp_translate );
    luaG_addfunction( L, "scale", shp_scale );
    luaG_addfunction( L, "wp", shp_wp );
    luaG_addfunction( L, "write_stl", shp_write_stl );
    lua_settable( L, -3 );
}
