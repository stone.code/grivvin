// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#ifndef _GRIVVIN_H_
#define _GRIVVIN_H_

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Polygon_set_2.h>
#include <CGAL/Nef_polyhedron_3.h>

namespace grivvin
{
	// Define kernel and basic geometric objects
	typedef CGAL::Exact_predicates_exact_constructions_kernel kernel;
	typedef kernel::FT field;

	typedef kernel::Point_2 point2d;
	typedef kernel::Point_3 point3d;
	typedef kernel::Vector_2 vector2d;
	typedef kernel::Vector_3 vector3d;
	typedef kernel::Plane_3 plane3d;
	typedef kernel::Aff_transformation_2 trans2d;
	typedef kernel::Aff_transformation_3 trans3d;

	// Contructed 2D geometry
	typedef CGAL::Polygon_set_2<kernel>	polygon;

	polygon create_path( field x1, field y1, field x2, field y2, field width );
	polygon	create_rect( field x1, field y1, field x2, field y2 );
	polygon	create_circle( field radius, field x, field y, unsigned points = 4 );
	polygon create_round_rect( field x1, field y1, field x2, field y2, field radius, unsigned points = 3 );
	polygon create_triangle( field x1, field y1, field x2, field y2, field x3, field y3 );
	inline polygon create_circle( field radius, point2d pt, unsigned points = 3 )
		{ return create_circle( radius, pt.x(), pt.y(), points ); }
	polygon	create_ellipse( field radius1, field readiu2, field x, field y, unsigned points = 3 );
	polygon	create_egg_2( field radius1, field radius2, field x, field y, field factor, unsigned points = 3 );

	polygon	operator+( polygon const&, polygon const& );
	polygon operator-( polygon const&, polygon const& );
	polygon operator*( polygon const&, polygon const& );
	polygon operator^( polygon const&, polygon const& );
	polygon scale( polygon const&, double s );
	polygon minkowski( polygon const&, polygon const& );

	// Contructed 2D geometry
	class workplane {
	public:
		point3d origin;
		vector3d dx, dy;
		
		workplane	scale( double s ) const;
		workplane	translate( vector3d v ) const;
	};
	
	// 3D construction
	typedef CGAL::Nef_polyhedron_3<kernel>	polyhedron;

	polyhedron	extrude( polygon p, workplane const& wp, vector3d dz );
	polyhedron	extrude( polygon p, workplane const& wp1, workplane const& wp2 );
	polyhedron	revolve( polygon p, workplane const& wp, unsigned points = 3 );
	polyhedron	revolve( polygon p, workplane const& wp, double theta1, double theta2, unsigned points = 3 );
	polyhedron	create_sphere( field radius, point3d center, unsigned points = 3 );
	polyhedron	create_cylinder( field radius, point3d a, point3d b, unsigned points = 3 );

	// Export
	void	write_ps2d( char const* filename, polygon const&, double scale );
	void	write_dxf2d( char const* filename, polygon const&, double scale );
	void	write_stl( char const* filename, polyhedron src );
}

#endif // _GRIVVIN_H_

