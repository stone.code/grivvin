// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#include <lua.hpp>
#include <cstring>
#include <memory>

#define CATCH_CONFIG_MAIN
#include "vendor/catch.hpp"

extern void point3d_register( lua_State *L );
extern void vector3d_register( lua_State *L );
extern void pgn_register( lua_State *L );
extern void shp_register( lua_State *L );
extern void wp_register( lua_State *L );

unsigned short curve_point_count = 1;

lua_State* init_lua_state()
{
	lua_State* L = 0;

#if LUA_VERSION_NUM > 501
	L = luaL_newstate();
#else
	L = lua_open();
#endif
	luaopen_base(L);
	luaopen_string(L);
	luaopen_math(L);
	point3d_register(L);
	vector3d_register(L);
	pgn_register( L );
	shp_register(L );
	wp_register(L);
	
	return L;
}

int exec_lua_script( lua_State* L, char const* source )
{
	int err = luaL_loadbuffer(L, source, std::strlen(source), "line");
	if (err) {
		return err;
	}
	err = lua_pcall( L, 0, 0, 0);
	if (err) {
		return err;
	}
	return err;
}
