// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "grivvin.h"
#include <assert.h>
#include <math.h>
#include <CGAL/Polygon_2.h>

using grivvin::field;
using grivvin::polygon;

polygon grivvin::create_path( field x1, field y1, field x2, field y2, field width )
{
	double theta = atan2( CGAL::to_double(y2-y1), CGAL::to_double(x2-x1) );
	width /= 2;
	
	CGAL::Polygon_2<kernel> pgn;
	pgn.push_back( point2d( x1 + width*cos(theta+M_PI_2), y1 + width*sin(theta+M_PI_2) ) );
	pgn.push_back( point2d( x1 + width*cos(theta-M_PI_2), y1 + width*sin(theta-M_PI_2) ) );
	pgn.push_back( point2d( x2 + width*cos(theta-M_PI_2), y2 + width*sin(theta-M_PI_2) ) );
	pgn.push_back( point2d( x2 + width*cos(theta+M_PI_2), y2 + width*sin(theta+M_PI_2) ) );

	assert( !pgn.is_empty() );
	assert( pgn.is_simple() );
	assert( pgn.is_convex() );
	assert( pgn.orientation() == CGAL::COUNTERCLOCKWISE );

	return CGAL::Polygon_set_2<kernel>( pgn );
}

polygon grivvin::create_rect( field x1, field y1, field x2, field y2 )
{
	assert( x1 < x2 );
	assert( y1 < y2 );

	CGAL::Polygon_2<kernel> pgn;
	pgn.push_back( point2d( x1, y1 ) );
	pgn.push_back( point2d( x2, y1 ) );
	pgn.push_back( point2d( x2, y2 ) );
	pgn.push_back( point2d( x1, y2 ) );
	assert( !pgn.is_empty() );
	assert( pgn.is_simple() );
	assert( pgn.is_convex() );
	assert( pgn.orientation() == CGAL::COUNTERCLOCKWISE );

	return CGAL::Polygon_set_2<kernel>( pgn );
}

polygon grivvin::create_round_rect( field x1, field y1, field x2, field y2, field radius, unsigned points )
{
	assert( x1 < x2 );
	assert( y1 < y2 );
	assert( (radius > 0) && (radius<(x2-x1)*0.5) && (radius<(y2-y1)*0.5) );
	
	CGAL::Polygon_2<kernel> pgn;

	if ( points==0 ) {
		pgn.push_back( point2d( x2, y2 - radius ) );
		pgn.push_back( point2d( x2 - radius, y2 ) );
		pgn.push_back( point2d( x1 + radius, y2 ) );
		pgn.push_back( point2d( x1, y2 - radius ) );
		pgn.push_back( point2d( x1, y1 + radius ) );
		pgn.push_back( point2d( x1 + radius, y1 ) );
		pgn.push_back( point2d( x2 - radius, y1 ) );
		pgn.push_back( point2d( x2, y1 + radius ) );
	}
	else {
		assert( points > 0 );
		
		x1 += radius; x2 -= radius;
		y1 += radius; y2 -= radius;
	
		std::vector<vector2d> pts;

		for ( unsigned lp=0; lp<=points; ++lp ) {
			double theta = lp * M_PI_2 / (points+1);
			double c = CGAL::to_double(radius) * cos( theta );
			double s = CGAL::to_double(radius) * sin( theta );
			pts.push_back( vector2d( c, s ) );
		}
		for ( unsigned lp=0; lp<=points; ++lp )
			pgn.push_back( point2d( x2 + pts[lp].x(), y2 + pts[lp].y() ) );
		pgn.push_back( point2d( x2, y2+radius ) );
		for ( unsigned lp=0; lp<=points; ++lp )
			pgn.push_back( point2d( x1 - pts[lp].y(), y2 + pts[lp].x() ) );
		pgn.push_back( point2d( x1-radius, y2 ) );
		for ( unsigned lp=0; lp<=points; ++lp )
			pgn.push_back( point2d( x1 - pts[lp].x(), y1 - pts[lp].y() ) );
		pgn.push_back( point2d( x1, y1-radius ) );
		for ( unsigned lp=0; lp<=points; ++lp )
			pgn.push_back( point2d( x2 + pts[lp].y(), y1 - pts[lp].x() ) );
		pgn.push_back( point2d( x2+radius, y1 ) );
	}

	assert( !pgn.is_empty() );
	assert( pgn.is_simple() );
	assert( pgn.is_convex() );
	assert( pgn.orientation() == CGAL::COUNTERCLOCKWISE );

	return CGAL::Polygon_set_2<kernel>( pgn );
}

polygon grivvin::create_circle( field radius, field x, field y, unsigned points )
{
	assert( radius > 0 );
	assert( points > 0 );

	std::vector<vector2d> pts;
	CGAL::Polygon_2<kernel> pgn;

	for ( unsigned lp=0; lp<=points; ++lp ) {
		double theta = lp * M_PI_2 / (points+1);
		double c = CGAL::to_double(radius) * cos( theta );
		double s = CGAL::to_double(radius) * sin( theta );
		pts.push_back( vector2d( c, s ) );
	}
	for ( unsigned lp=0; lp<=points; ++lp )
		pgn.push_back( point2d( x + pts[lp].x(), y + pts[lp].y() ) );
	for ( unsigned lp=0; lp<=points; ++lp )
		pgn.push_back( point2d( x - pts[lp].y(), y + pts[lp].x() ) );
	for ( unsigned lp=0; lp<=points; ++lp )
		pgn.push_back( point2d( x - pts[lp].x(), y - pts[lp].y() ) );
	for ( unsigned lp=0; lp<=points; ++lp )
		pgn.push_back( point2d( x + pts[lp].y(), y - pts[lp].x() ) );

	assert( !pgn.is_empty() );
	assert( pgn.is_simple() );
	assert( pgn.is_convex() );
	assert( pgn.orientation() == CGAL::COUNTERCLOCKWISE );

	return CGAL::Polygon_set_2<kernel>( pgn );
}

polygon grivvin::create_triangle( field x1, field y1, field x2, field y2, field x3, field y3 )
{
	CGAL::Polygon_2<kernel> pgn;
	pgn.push_back( point2d( x1, y1 ) );
	pgn.push_back( point2d( x2, y2 ) );
	pgn.push_back( point2d( x3, y3 ) );
	
	if ( pgn.orientation() != CGAL::COUNTERCLOCKWISE ) {
		pgn.reverse_orientation();
	}

	assert( !pgn.is_empty() );
	assert( pgn.is_simple() );
	assert( pgn.is_convex() );
	assert( pgn.orientation() == CGAL::COUNTERCLOCKWISE );

	return CGAL::Polygon_set_2<kernel>( pgn );
}

polygon grivvin::create_ellipse( field radius1, field radius2, field x, field y, unsigned points )
{
	assert( radius1 > 0 );
	assert( radius2 > 0 );
	assert( points > 0 );

	CGAL::Polygon_2<kernel> pgn;

	for ( unsigned lp=0; lp<points*4; ++lp ) {
		double theta = lp * M_PI_2 / points;
		double c = CGAL::to_double(radius1) * cos( theta );
		double s = CGAL::to_double(radius2) * sin( theta );
		pgn.push_back( point2d( x+c, y+s ) );
	}

	assert( !pgn.is_empty() );
	assert( pgn.is_simple() );
	assert( pgn.is_convex() );

	return CGAL::Polygon_set_2<kernel>( pgn );
}

polygon	grivvin::create_egg_2( field radius1, field radius2, field x, field y, field factor_, unsigned points )
{
	assert( radius1 > 0 );
	assert( radius2 > 0 );
	assert( factor_ >= 0 );
	assert( points > 0 );

	CGAL::Polygon_2<kernel> pgn;
	double factor = CGAL::to_double(factor_ / radius1 );

	for ( unsigned lp=0; lp<points*4; ++lp ) {
		double theta = lp * M_PI_2 / points;
		double c = CGAL::to_double(radius1) * cos( theta );
		double s = CGAL::to_double(radius2) * sin( theta );
		pgn.push_back( point2d( x+c, y + s*(1+factor*c) ) );
	}

	assert( !pgn.is_empty() );
	assert( pgn.is_simple() );
	assert( pgn.is_convex() );

	return CGAL::Polygon_set_2<kernel>( pgn );
}
