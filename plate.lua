-- Start
print("Creating the part")
set_point_count(5)

inch = 25.4
plate_size = 6 * inch

-- 2D
poly = pgn:rect( inch/16, inch/16, (3-3/16)*inch, inch*3/16 )
poly = poly + pgn:path( (3-3/16)*inch, inch*2/16, (3-1/16)*inch, inch*6/16, inch/8 )
poly = poly + pgn:circle( inch/16, (3-3/16)*inch, inch*2/16 )
poly = poly + pgn:circle( inch/16, (3-1/16)*inch, inch*6/16 )
poly = poly + pgn:rect( (3-9/16)*inch, 0, (3-7/16)*inch, inch/8 ) + pgn:tri( (3-9/16)*inch, 0, (3-11/16)*inch, inch/8, (3-9/16)*inch, inch/8 )
poly = poly + pgn:tri( (3-7/16)*inch, 0, (3-5/16)*inch, inch/8, (3-7/16)*inch, inch/8 )

-- 3D
set_point_count(1)
shape = workplane("xy"):revolve( poly )
-- shape = workplane("xy"):extrude( poly, vector3d(0,0,inch/8) )


-- Write
print("Writing output.")
shape:write_stl("plate.stl")
