// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#ifndef _GRIVVIN_TRIANGULATE_FACE_H_
#define _GRIVVIN_TRIANGULATE_FACE_H_

#include "grivvin.h"
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

namespace grivvin {
namespace face_triangulation_ns {
	typedef CGAL::Triangulation_vertex_base_with_info_2<polyhedron::Vertex_const_handle,kernel> Vb;
	typedef CGAL::Triangulation_face_base_with_info_2<bool,Vb> FbBase;
	typedef CGAL::Constrained_triangulation_face_base_2<kernel,FbBase> Fb;
	typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;
	typedef CGAL::Constrained_Delaunay_triangulation_2<kernel,Tds> triangulation;
}

typedef face_triangulation_ns::triangulation face_triangulation;

void construct_face_triangulation( face_triangulation& tri, polyhedron::Halffacet_const_handle hf );

}

#endif // _GRIFFIN_TRIANGULATE_FACE_H_

