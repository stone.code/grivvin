// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#ifndef _GRIVVIN_LUAG_H
#define _GRIVVIN_LUAG_H

#include <cassert>
#include <lua.hpp>
#include <typeinfo>

void luaG_addfunction( lua_State *L, char const* name, lua_CFunction fn );
void luaG_checkargcount( lua_State *L, int c );
int  luaG_checkutype( lua_State *L, int c, char const* tname );

template <typename T>
struct luaG_traits
{
    static char const* const metatable;
};


template <typename T>
T* luaG_checkudata( lua_State* L, int index )
{
	void* ud = luaL_checkudata(L,index,luaG_traits<T>::metatable);
	return reinterpret_cast<T*>(ud);
}

template <typename T>
void luaG_push( lua_State *L, T const& rhs )
{
	void* luaobj = lua_newuserdata(L,sizeof(T));
	new(luaobj) T(rhs);
	luaL_getmetatable(L,luaG_traits<T>::metatable);
	lua_setmetatable(L,-2);
}

#endif
