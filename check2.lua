-- Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
-- Use of this source code is governed the license found in the LICENSE file.


set_point_count(3)

shape = shp:block( -10, -10, -10, 10, 10, 10 )
shape = shape + shp:sphere( 10, point3d(0,0,10 ) )
shape = shape + shp:cylinder( 10, point3d(0,0,0), point3d(25,0,0) )

-- Write
print("Writing output.")
shape:write_stl("check2.stl")
