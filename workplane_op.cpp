// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "grivvin.h"
#include <assert.h>

using grivvin::vector3d;
using grivvin::workplane;

workplane workplane::scale( double s ) const
{
	grivvin::trans3d transform( CGAL::SCALING, s );

	workplane ret;
	
	ret.origin = origin;
	ret.dx = transform( dx );
	ret.dy = transform( dy );
	return ret;
}

workplane workplane::translate( vector3d v ) const
{
	workplane ret;
	
	ret.origin = origin + v;
	ret.dx = dx;
	ret.dy = dy;
	return ret;
}
