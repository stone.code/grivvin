AC_DEFUN(AC_UNDEFINE,[
cp confdefs.h confdefs.h.tmp
grep -v $1 < confdefs.h.tmp > confdefs.h
rm confdefs.h.tmp
])

AC_DEFUN(AC_CXX_CHECK_LIB,[
saved_libs="${LIBS}"
LIBS="-l$1 ${saved_libs}"
AC_MSG_CHECKING([for -l$1])
AC_LINK_IFELSE([$2],
	[AC_MSG_RESULT([yes])]
	[LIBS="${saved_libs}"
	$3],
	[AC_MSG_RESULT([no])]
	[LIBS="${saved_libs}"
	$4])
])

AC_DEFUN([AC_CHECK_CXXFLAG],[
AC_MSG_CHECKING([whether compiler accepts "$1"])
cat > conftest.c++ << EOF
int main(){
  return 0;
}
EOF
if $CXX $CPPFLAGS $CXXFLAGS [$1] -o conftest.o conftest.c++ > /dev/null 2>&1
then
    AC_MSG_RESULT([yes])
    CXXFLAGS="${CXXFLAGS} [$1]"
    [$2]
else
    AC_MSG_RESULT([no])
    [$3]
fi
])

AC_DEFUN([AC_ENABLE_COVERAGE],[
AC_ARG_ENABLE([cover],AS_HELP_STRING([--enable-cover],[Use coverage profiling when running 'make check' (default is no)]),[use_cover=$withval],[use_cover=no])
if test x$use_cover = xno; then
	AC_MSG_NOTICE([Coverage profiling disabled])
else
	AC_CHECK_LIB([gcov],[__gcov_init],[],[use_cover=no])
	AC_CHECK_PROG([HAVE_LCOV],[lcov],[yes],[no])
	AC_CHECK_PROG([HAVE_GENHTML],[genhtml],[yes],[no])
	if test x$HAVE_LCOV = xno; then
		use_cover=no
	fi;
	if test x$HAVE_GENHTML = xno; then
		use_cover=no
	fi;
	if test x$use_cover = xno; then
		AC_MSG_WARN([Coverage profiling disabled])
	else
		AC_MSG_NOTICE([Enabling coverage profiling])
		use_cover=yes
		CXXFLAGS="$CXXFLAGS -fprofile-arcs -ftest-coverage"
	fi;
fi;
AC_SUBST([use_cover],"x$use_cover")
])
