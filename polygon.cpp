// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "luag.h"
#include <math.h>
#include "grivvin.h"

using grivvin::point3d;
using grivvin::polygon;

extern unsigned short curve_point_count;

template <>
char const* const luaG_traits<polygon>::metatable = "grivvin.polygon";

static int pgn_rect_5(lua_State *L)
{
	lua_Number x1, y1, x2, y2;
	x1 = luaL_checknumber( L, 2 );
	y1 = luaL_checknumber( L, 3 );
	x2 = luaL_checknumber( L, 4 );
	y2 = luaL_checknumber( L, 5 );

	polygon pgn = grivvin::create_rect( x1, y1, x2, y2 );
	luaG_push( L, pgn );
	return 1;
}

static int pgn_rect_6(lua_State *L)
{
	lua_Number x1, y1, x2, y2, r;
	x1 = luaL_checknumber( L, 2 );
	y1 = luaL_checknumber( L, 3 );
	x2 = luaL_checknumber( L, 4 );
	y2 = luaL_checknumber( L, 5 );
	r = luaL_checknumber( L, 6 );

	polygon pgn = grivvin::create_round_rect( x1, y1, x2, y2, r, curve_point_count );
	luaG_push( L, pgn );
	return 1;
}

static int pgn_rect(lua_State *L)
{
	switch (lua_gettop(L)) {
		case 5:
			return pgn_rect_5(L);
		case 6:
			return pgn_rect_6(L);
	}

	return luaL_error(L,"incorrect number of arguments in call to rect");
}

static int pgn_circle_2(lua_State *L)
{
	lua_Number r = luaL_checknumber( L, 2 );

	polygon pgn = grivvin::create_circle( r, 0, 0, curve_point_count );
	luaG_push( L, pgn );
	return 1;
}

static int pgn_circle_4(lua_State *L)
{
	lua_Number r = luaL_checknumber( L, 2 );
	lua_Number x = luaL_checknumber( L, 3 );
	lua_Number y = luaL_checknumber( L, 4 );

	polygon pgn = grivvin::create_circle( r, x, y, curve_point_count );
	luaG_push( L, pgn );
	return 1;
}

static int pgn_circle(lua_State *L)
{
	switch (lua_gettop(L)) {
		case 2:
			return pgn_circle_2(L);
		case 4:
			return pgn_circle_4(L);
	}

	return luaL_error(L,"incorrect number of arguments in call to circle");
}

static int pgn_ellipse(lua_State *L)
{
	luaG_checkargcount(L,5);
	lua_Number r1 = luaL_checknumber( L, 2 );
	lua_Number r2 = luaL_checknumber( L, 3 );
	lua_Number x = luaL_checknumber( L, 4 );
	lua_Number y = luaL_checknumber( L, 5 );

	polygon pgn = grivvin::create_ellipse( r1, r2, x, y, curve_point_count );
	luaG_push( L, pgn );
	return 1;
}

static int pgn_tri(lua_State *L)
{
	luaG_checkargcount(L,7);
	lua_Number x1 = luaL_checknumber( L, 2 );
	lua_Number y1 = luaL_checknumber( L, 3 );
	lua_Number x2 = luaL_checknumber( L, 4 );
	lua_Number y2 = luaL_checknumber( L, 5 );
	lua_Number x3 = luaL_checknumber( L, 6 );
	lua_Number y3 = luaL_checknumber( L, 7 );

	polygon pgn = grivvin::create_triangle( x1, y1, x2, y2, x3, y3 );
	luaG_push( L, pgn );
	return 1;
}

static int pgn_path(lua_State *L)
{
	luaG_checkargcount(L,6);
	lua_Number x1 = luaL_checknumber(L,2);
	lua_Number y1 = luaL_checknumber(L,3);
	lua_Number x2 = luaL_checknumber(L,4);
	lua_Number y2 = luaL_checknumber(L,5);
	lua_Number w = luaL_checknumber(L,6);

	polygon pgn = grivvin::create_path( x1, y1, x2, y2, w );
	luaG_push( L, pgn );
	return 1;
}

static int pgn_gc(lua_State *L)
{
	polygon* ud = luaG_checkudata<polygon>(L,1);
	ud->~polygon();
	return 0;
}

static int pgn_add(lua_State *L)
{
	luaG_checkargcount(L,2);
	polygon* pgn1 = luaG_checkudata<polygon>(L,1);
	polygon* pgn2 = luaG_checkudata<polygon>(L,2);

	polygon ret;
	ret.join( *pgn1, *pgn2);
	luaG_push( L, ret );
	return 1;
}

static int pgn_sub(lua_State *L)
{
	luaG_checkargcount(L,2);
	polygon* pgn1 = luaG_checkudata<polygon>(L,1);
	polygon* pgn2 = luaG_checkudata<polygon>(L,2);

	polygon ret;
	ret.difference( *pgn1, *pgn2 );
	luaG_push( L, ret );
	return 1;
}

static int pgn_mul(lua_State *L)
{
	luaG_checkargcount(L,2);
	polygon* pgn1 = luaG_checkudata<polygon>(L,1);
	polygon* pgn2 = luaG_checkudata<polygon>(L,2);

	polygon ret;
	ret.intersection( *pgn1, *pgn2 );
	luaG_push( L, ret );
	return 1;
}

static int pgn_isempty(lua_State *L)
{
	luaG_checkargcount(L,1);
	polygon* ud = luaG_checkudata<polygon>(L,1);
	bool const ret = ud->is_empty();
	lua_pushboolean(L,ret);
	return 1;
}

static int pgn_locate(lua_State *L)
{
	luaG_checkargcount(L,3);
	polygon* ud = luaG_checkudata<polygon>(L,1);
	lua_Number x = luaL_checknumber(L,2);
	lua_Number y = luaL_checknumber(L,3);

	CGAL::Oriented_side const ret = ud->oriented_side(grivvin::point2d(x,y));
	lua_pushnumber(L,ret);
	return 1;
}

static int pgn_write_ps_2(lua_State *L)
{
	polygon* ud = luaG_checkudata<polygon>(L,1);
	char const* filename = luaL_checkstring(L,2);

	grivvin::write_ps2d( filename, *reinterpret_cast<polygon*>(ud), 1 );
	return 0;
}

static int pgn_write_ps_3(lua_State *L)
{
	polygon* ud = luaG_checkudata<polygon>(L,1);
	char const* filename = luaL_checkstring(L,2);
	lua_Number scale = luaL_checknumber( L,3);

	grivvin::write_ps2d( filename, *reinterpret_cast<polygon*>(ud), scale );
	return 0;
}

static int pgn_write_ps(lua_State *L)
{
	switch (lua_gettop(L)) {
		case 2:
			return pgn_write_ps_2(L);
		case 3:
			return pgn_write_ps_3(L);
	}

	return luaL_error(L,"incorrect number of arguments in call to write_ps");
}

static int pgn_write_dxf_2(lua_State *L)
{
	polygon* ud = luaG_checkudata<polygon>(L,1);
	char const* filename = luaL_checkstring(L,2);

	grivvin::write_dxf2d( filename, *ud, 1 );
	return 0;
}

static int pgn_write_dxf_3(lua_State *L)
{
	polygon* ud = luaG_checkudata<polygon>(L,1);
	char const* filename = luaL_checkstring(L,2);
	lua_Number scale = luaL_checknumber( L,3);

	grivvin::write_dxf2d( filename, *ud, scale );
	return 0;
}

static int pgn_write_dxf(lua_State *L)
{
	switch (lua_gettop(L)) {
		case 2:
			return pgn_write_dxf_2(L);
		case 3:
			return pgn_write_dxf_3(L);
	}

	return luaL_error(L,"incorrect number of arguments in call to write_dxf");
}

static polygon::Polygon_2 apply_transform( polygon::Polygon_2 const& shape, grivvin::trans2d t )
{
	polygon::Polygon_2 ret;

	for ( size_t lp=0; lp<shape.size(); ++lp ) {
		ret.push_back( t.transform( shape[lp] ) );
	}

	return ret;
}

static void apply_transform( polygon& out, polygon const& shape, grivvin::trans2d t )
{
	// Get the polygons from the shape
	std::vector<polygon::Polygon_with_holes_2> pwhs;
	shape.polygons_with_holes( std::back_inserter( pwhs ) );

	// Each polygon, transform
	for ( size_t lp=0; lp<pwhs.size(); ++lp ) {
		polygon::Polygon_with_holes_2 const& pgn = pwhs[lp];

		// outer boundary
		polygon::Polygon_2 outer = apply_transform( pgn.outer_boundary(), t );
		std::vector<polygon::Polygon_2> holes;

		// Substract volumes for holes
		for ( polygon::Polygon_with_holes_2::Hole_const_iterator h = pgn.holes_begin();
			h != pgn.holes_end(); ++h ) {
			holes.push_back( apply_transform( *h, t ) );
		}

		out.insert( polygon::Polygon_with_holes_2( outer, holes.begin(), holes.end() ) );
	}
}

static int pgn_rotate(lua_State *L)
{
	luaG_checkargcount(L,2);
	polygon* pgn1 = luaG_checkudata<polygon>(L,1);
	lua_Number theta = luaL_checknumber( L, 2 );

	double const hw = 1000;
	grivvin::trans2d t( CGAL::ROTATION, hw * sin(theta), hw * cos(theta), hw );

	polygon ret;
	apply_transform( ret, *pgn1, t );
	luaG_push( L, ret );
	return 1;
}

static int pgn_translate(lua_State *L)
{
	luaG_checkargcount(L,3);
	polygon* pgn1 = luaG_checkudata<polygon>(L,1);
	lua_Number x = luaL_checknumber( L, 2 );
	lua_Number y = luaL_checknumber( L, 3 );

	grivvin::trans2d t( CGAL::TRANSLATION, grivvin::vector2d(x, y) );

	polygon ret;
	apply_transform( ret, *pgn1, t );
	luaG_push( L, ret );
	return 1;
}

static luaL_Reg pgn_fns[] = {
	{ "rect", pgn_rect },
	{ "circle", pgn_circle },
	{ "ellipse", pgn_ellipse },
	{ "path", pgn_path },
	{ "tri", pgn_tri },
	{ 0, 0 }
};

void pgn_register( lua_State *L )
{
#if LUA_VERSION_NUM > 501
    lua_newtable(L);
    luaL_setfuncs (L,pgn_fns,0);
    lua_pushvalue(L,-1);        // pluck these lines out if they offend you
    lua_setglobal(L,"pgn"); // for they clobber the Holy _G
#else
    luaL_register(L,"pgn",pgn_fns);
#endif

	luaL_newmetatable(L,"grivvin.polygon");
	lua_pushstring(L, "__index");
	lua_pushvalue(L,-2); /* pushes the metatable */
	lua_settable(L,-3); /*metatable.__index = metatable */
	luaG_addfunction(L,"__gc",pgn_gc);
	luaG_addfunction(L, "__add",pgn_add);
	luaG_addfunction(L, "__sub",pgn_sub);
	luaG_addfunction(L, "__mul",pgn_mul);
	luaG_addfunction(L, "isempty", pgn_isempty);
	luaG_addfunction(L, "locate", pgn_locate);
	luaG_addfunction(L, "rotate", pgn_rotate);
	luaG_addfunction(L, "translate", pgn_translate);
	luaG_addfunction(L, "write_ps", pgn_write_ps);
	luaG_addfunction(L, "write_dxf", pgn_write_dxf);
}
