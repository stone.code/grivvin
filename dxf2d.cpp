// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.
     
#include "grivvin.h"

using namespace grivvin;

static void write_HEADER( std::ostream& out )
{
	out << "  0\n";
	out << "SECTION\n";
	out << "  2\n";
	out << "HEADER\n";
	out << "  0\n";
	out << "ENDSEC\n";
}

static void write_start_ENTITIES( std::ostream& out )
{
	out << "  0\n";
	out << "SECTION\n";
	out << "  2\n";
	out << "ENTITIES\n";
}

static void write_end_ENTITIES( std::ostream& out )
{
	out << "  0\n";
	out << "ENDSEC\n";
}

static void write_EOF( std::ostream& out )
{
	out << "  0\n";
	out << "EOF\n";
}

static void write_point( std::ostream& out, point2d const& pt, double scale, bool final_point )
{
	// Is there a possible loss of precision in the conversion below?  i.e., instead
	// of pt.x()*scale, the coordinate is converted a double and then multiplied.
	// Since the value must be converted to a double before being written to the DXF,
	// it should be okay.  Not certain that we are getting full precision when writing
	// the double either.
	
	out << "  10\n";
	out << (CGAL::to_double(pt.x())*scale) << '\n';
	out << "  20\n";
	out << (CGAL::to_double(pt.y())*scale) << '\n';
	out << "  30\n0\n";
	if ( final_point )
		out << "  0\nSEQEND\n";
	else
		out << "  0\nVERTEX\n";
}

static void write_polygon( std::ostream& out, CGAL::Polygon_2<kernel> const& pgn, double scale )
{
	out << "  0\nPOLYLINE\n"; // start of a polyline
	out << "  70\n1\n"; // flag indicating closed polyline (polygon)
	out << "  8\n" "LAYER" "\n";

	// write out the polygon points
	for( size_t lp=0; lp<pgn.size(); ++lp )
		write_point( out, pgn[lp], scale, false );
	if ( pgn.size()>0 ) 
		write_point( out, pgn[0], scale, true );
}

static void write_polygon_with_holes( std::ostream& out, CGAL::Polygon_with_holes_2<kernel> const& pgn, double scale)
{
	// outer boundary
	write_polygon( out, pgn.outer_boundary(), scale );
	// Substract volumes for holes
	for ( CGAL::Polygon_with_holes_2<kernel>::Hole_const_iterator h = pgn.holes_begin();
		h != pgn.holes_end(); ++h ) {
		write_polygon( out, *h, scale );
	}
}

void	grivvin::write_dxf2d( char const* filename, polygon const& shape, double scale )
{
	std::vector<CGAL::Polygon_with_holes_2<kernel> > pwhs;
	shape.polygons_with_holes( std::back_inserter( pwhs ) );

	// Open the output file
	std::ofstream file( filename );

	// write the file header
	write_HEADER( file );

	// draw the shapes
	write_start_ENTITIES( file );
	for ( size_t lp=0; lp<pwhs.size(); ++lp ) {
    	// Each polygon
		CGAL::Polygon_with_holes_2<kernel> const& pgn = pwhs[lp];
		write_polygon_with_holes( file, pgn, scale );
	}
	write_end_ENTITIES( file );

	// complete the file
	write_EOF( file );
}
