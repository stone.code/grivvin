// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include <lua.hpp>
#include "grivvin.h"
#include "luag.h"

using grivvin::point3d;
using grivvin::vector3d;

template <>
char const* const luaG_traits<point3d>::metatable = "grivvin.point3d";

static int point3d_new(lua_State *L) 
{
	luaG_checkargcount( L, 3 );
	lua_Number x = luaL_checknumber( L, 1 );
	lua_Number y = luaL_checknumber( L, 2 );
	lua_Number z = luaL_checknumber( L, 3 );
	
	luaG_push(L, point3d(x,y,z) );
	return 1;
}

static int point3d_gc(lua_State *L)
{
	point3d* ud = luaG_checkudata<point3d>(L,1);
	ud->~point3d();
	return 0;
}

static int point3d_eq(lua_State *L) 
{
	point3d* ud1 = luaG_checkudata<point3d>(L,1);
	point3d* ud2 = luaG_checkudata<point3d>(L,2);
	
	lua_pushboolean( L, *ud1 == *ud2 );
	return 1;
}
	
static int point3d_add(lua_State *L)
{
	point3d* ud1 = luaG_checkudata<point3d>(L,1);
	vector3d* ud2 = luaG_checkudata<vector3d>(L,2);
	
	luaG_push( L, *ud1 	+ *ud2 );
	return 1;
	
}

static int point3d_sub(lua_State *L)
{
	point3d* ud1 = luaG_checkudata<point3d>(L,1);
	vector3d* ud2 = luaG_checkudata<vector3d>(L,2);
	
	luaG_push( L, *ud1 - *ud2 );
	return 1;
	
}

static int point3d_tostring(lua_State *L)
{
	luaG_checkargcount( L, 1 );
	point3d* ud = luaG_checkudata<point3d>(L,1);
	char buffer[ (1+2+10+1+2)*3 + 4 + 1];
	
	int n = snprintf( buffer, sizeof(buffer), "(%g,%g,%g)", CGAL::to_double(ud->x()), CGAL::to_double(ud->y()), CGAL::to_double(ud->z()) );
	assert( n>=0 && static_cast<size_t>(n)<sizeof(buffer) );
	lua_pushstring(L,buffer);
	return 1;
}

static int point3d_iszero(lua_State *L)
{
	luaG_checkargcount( L, 1 );
	point3d* ud = luaG_checkudata<point3d>(L,1);
	
	bool const ret = ud->x()==0 && ud->y()==0 && ud->z()==0;
	lua_pushboolean(L,ret);
	return 1;
}

static int point3d_x(lua_State *L)
{
	luaG_checkargcount(L,1);
	point3d* ud1 = luaG_checkudata<point3d>(L,1);
	lua_pushnumber( L, CGAL::to_double( ud1->x() ) );
	return 1;
	
}

static int point3d_y(lua_State *L)
{
	luaG_checkargcount(L,1);
	point3d* ud1 = luaG_checkudata<point3d>(L,1);
	lua_pushnumber( L, CGAL::to_double( ud1->y() ) );
	return 1;
	
}

static int point3d_z(lua_State *L)
{
	luaG_checkargcount(L,1);
	point3d* ud1 = luaG_checkudata<point3d>(L,1);
	lua_pushnumber( L, CGAL::to_double( ud1->z() ) );
	return 1;
	
}

void point3d_register( lua_State *L )
{
	luaL_newmetatable(L,luaG_traits<point3d>::metatable);
	lua_pushstring(L, "__index");
	lua_pushvalue(L,-2); /* pushes the metatable */
	lua_settable(L,-3); /*metatable.__index = metatable */
	luaG_addfunction( L, "__gc", point3d_gc );
	luaG_addfunction( L, "__eq", point3d_eq );
	luaG_addfunction( L, "__add", point3d_add );
	luaG_addfunction( L, "__sub", point3d_sub );
	luaG_addfunction( L, "__tostring", point3d_tostring );
	luaG_addfunction( L, "iszero", point3d_iszero );
	luaG_addfunction( L, "x", point3d_x );
	luaG_addfunction( L, "y", point3d_y );
	luaG_addfunction( L, "z", point3d_z );
	
	lua_register( L, "point3d", point3d_new );
}
