// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "grivvin.h"
#include "triangulate_face.h"
#include <assert.h>
#include <fstream>
#include <iostream>

using grivvin::polyhedron;
using grivvin::face_triangulation;

static void write_header( std::ostream& out )
{
	out << "solid " PACKAGE_NAME "\n";
}

static void write_footer( std::ostream& out )
{
	out << "endsolid " PACKAGE_NAME "\n";
}

static void write_halffacet( std::ostream& out, polyhedron::Halffacet_const_handle hf )
{
	// Construct a triangulation of the half-facet
	// We want to reverse the order of vertices so that they
	// are properly oriented
	face_triangulation tri;
	grivvin::construct_face_triangulation( tri, hf->twin() );
	assert( tri.number_of_faces() > 0 );
	assert( tri.number_of_vertices() >= 3 );
	
	// Write out the marked triangles
	for ( face_triangulation::All_faces_iterator face = tri.all_faces_begin();
		face != tri.all_faces_end(); ++face ) {
		if ( !face->info() ) continue;
		
		assert( !tri.is_infinite( face ) );
		
		out << "facet normal 0 0 0\n";
		out << "  outer loop\n";

		for ( unsigned lp = 0; lp < 3 ; ++lp ) {
			double x = CGAL::to_double( face->vertex(lp)->info()->point().x() );
			double y = CGAL::to_double( face->vertex(lp)->info()->point().y() );
			double z = CGAL::to_double( face->vertex(lp)->info()->point().z() );
		out << "    vertex " << x << ' ' << y << ' ' << z << '\n'; 
		}

		out << "  endloop\n";
		out << "endfacet\n";
	}
}

static void write_polyhedron( std::ostream& out, polyhedron p )
{
	polyhedron::Halffacet_const_iterator lp;
	for ( lp = p.halffacets_begin(); lp != p.halffacets_end(); ++lp ) {
		// Need to process only faccets for marked volumes
		if ( ! lp->incident_volume()->mark() ) continue;
		// Write out this half-facet
		write_halffacet( out, lp );
	}
}

void grivvin::write_stl( char const* filename, polyhedron n )
{
	assert( !n.is_empty() );
	assert( !n.is_space() );
	assert( n == n.closure() );
	assert( n.is_valid() );

	std::ofstream file( filename );

	write_header( file );
	write_polyhedron( file, n );
	write_footer( file );
}

