// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include "grivvin.h"

using namespace grivvin;

#define MARGIN (18.0)

static void calc_ps_bounds( std::vector<CGAL::Polygon_with_holes_2<kernel> > const& pwhs, double& ps_xmin, double& ps_xmax, double& ps_ymin, double& ps_ymax )
{
	ps_xmin = CGAL::to_double(pwhs[0].outer_boundary()[0].x());
	ps_xmax = ps_xmin;
	ps_ymin = CGAL::to_double(pwhs[0].outer_boundary()[0].y());
	ps_ymax = ps_ymin;

	for ( size_t lp=0; lp<pwhs.size(); ++lp ) {
		CGAL::Polygon_2<kernel> const& pgn = pwhs[lp].outer_boundary();
		for ( size_t lp2=0; lp2<pgn.size(); ++lp2 ) {
			double x = CGAL::to_double(pgn[lp2].x());
			double y = CGAL::to_double(pgn[lp2].y());
			
			if ( x < ps_xmin ) ps_xmin = x;
			if ( x > ps_xmax ) ps_xmax = x;
			if ( y < ps_ymin ) ps_ymin = y;
			if ( y > ps_ymax ) ps_ymax = y;
		}
	}
}	

static void write_header( std::ostream& out, char const* title, double xmin, double ymin, double xmax, double ymax )
{
	assert( xmin < xmax );
	assert( ymin < ymax );

	// document header
	out << "%!PS-Adobe-2.0\n";
	out << "%%BoundingBox:  0 0 " << ceil(xmax-xmin+2*MARGIN) << ' ' << ceil(ymax-ymin+2*MARGIN) << '\n'; 
	out << "%%Creator: " PACKAGE_NAME " " PACKAGE_VERSION "\n";
	//out << "%%CreationDate:  " << universaltime() << '\n';
	if ( title && *title ) out << "%%Title:  " << title << '\n';
	out << "%%Pages: 1\n";
	out << "%%EndComments\n";

	// start of the only page
	out << "%%Page:  1 1\n";
	// add a pagesize directive (add 1pt wide margin)
	out << "<< /PageSize [" << ((xmax-xmin) + 2*MARGIN) << ' ' 
		<< ((ymax-ymin) + 2*MARGIN) << "] >> setpagedevice\n";
	out << "gsave\n";
	out << "0 setgray 1.0 setlinewidth 1 setlinejoin\n";

	// translate so that image is in upper right quadrant
	// offset for the 1pt margin
	assert( xmin <= xmax );
	assert( ymin <= ymax );
	out << (-xmin+MARGIN) << ' ' << (-ymin+MARGIN) << " translate\n";
}

static void write_footer( std::ostream& out ) // goes after last page
{
	// end of the only page
	out << "grestore\n";
	out << "showpage\n";

	// end of the document
	out << "%%EOF" << std::endl;
}

static void write_polygon( std::ostream& out, CGAL::Polygon_2<kernel> const& pgn, double scale )
{
	out << CGAL::to_double(pgn[0].x())*scale << ' ' << CGAL::to_double(pgn[0].y())*scale << " moveto\n";
	for ( size_t lp=1; lp<pgn.size(); ++lp ) {
		out << CGAL::to_double(pgn[lp].x())*scale << ' ' << CGAL::to_double(pgn[lp].y())*scale << " lineto\n";
	}
	out << "closepath\n";
}

static void write_polygon_with_holes( std::ostream& out, CGAL::Polygon_with_holes_2<kernel> const& pgn, double scale )
{
	// outer boundary
	write_polygon( out, pgn.outer_boundary(), scale );
	// Substract volumes for holes
	for ( CGAL::Polygon_with_holes_2<kernel>::Hole_const_iterator h = pgn.holes_begin();
		h != pgn.holes_end(); ++h ) {
		write_polygon( out, *h, scale );
	}
	out << "gsave 0.5 setgray fill grestore stroke\n";
}

void	grivvin::write_ps2d( char const* filename, polygon const& shape, double scale )
{
	std::vector<CGAL::Polygon_with_holes_2<kernel> > pwhs;
	shape.polygons_with_holes( std::back_inserter( pwhs ) );

	// Convert scale so that it translates meters to points
	scale *= 72 / 0.0254;

	// Get the xy bounds for the image
	double ps_xmin, ps_xmax, ps_ymin, ps_ymax;
	calc_ps_bounds( pwhs, ps_xmin, ps_xmax, ps_ymin, ps_ymax );
	ps_xmin *= scale;
	ps_xmax *= scale;
	ps_ymin *= scale;
	ps_ymax *= scale;

	// Open the output file
	std::ofstream file( filename );
	write_header( file, "grivvin", ps_xmin, ps_ymin, ps_xmax, ps_ymax );

	// Each polygon
	for ( size_t lp=0; lp<pwhs.size(); ++lp ) {
		CGAL::Polygon_with_holes_2<kernel> const& pgn = pwhs[lp];
		write_polygon_with_holes( file, pgn, scale );
	}
	write_footer( file );
}
