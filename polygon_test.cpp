// Copyright (c) 2016 Robert W. Johnstone. All rights reserved.
// Use of this source code is governed the license found in the LICENSE file.

#include <lua.hpp>
#include <cstring>
#include <memory>

#include "vendor/catch.hpp"

extern lua_State* init_lua_state();
extern int exec_lua_script( lua_State* L, char const* source );

TEST_CASE( "polygon" )
{
	SECTION( "rect" ) {
		char const* script =
			"v = pgn:rect( 0, 1, 2, 3)\n"
			"ret = not v:isempty()\n"
			"t1 = v:locate(1,2)\n"
			"t2 = v:locate(-1,2)\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );

		lua_getglobal(L, "ret");
		CHECK( lua_isboolean(L,-1) );
		CHECK( lua_toboolean(L,-1) );
		lua_getglobal(L, "t1");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
		lua_getglobal(L, "t2");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
	}

	SECTION( "rect" ) {
		char const* script =
			"v = pgn:rect( 0, 0, 1, 1, 0.1 )\n"
			"ret = not v:isempty()\n"
			"t1 = v:locate(0.5,0.5)\n"
			"t2 = v:locate(1,1)\n"
			"t3 = v:locate(0.9,0.9)\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );

		lua_getglobal(L, "ret");
		CHECK( lua_isboolean(L,-1) );
		CHECK( lua_toboolean(L,-1) );
		lua_getglobal(L, "t1");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
		lua_getglobal(L, "t2");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
		lua_getglobal(L, "t3");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
	}

	SECTION( "rect" ) {
		char const* script =
			"v = pgn:rect( 0, 0, 1 )\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		CHECK( err == LUA_ERRRUN );
	}

	SECTION( "circle" ) {
		char const* script =
			"v = pgn:circle( 1 )\n"
			"t1 = v:locate(0,0)\n"
			"t2 = v:locate(1,1)\n"
			"t3 = v:locate(-1,-1)\n"
			"t4 = v:locate(1.5,0)\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );

		lua_getglobal(L, "t1");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
		lua_getglobal(L, "t2");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
		lua_getglobal(L, "t3");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
		lua_getglobal(L, "t4");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
	}

	SECTION( "circle" ) {
		char const* script =
			"v = pgn:circle( 1, 1, 1 )\n"
			"t1 = v:locate(1,1)\n"
			"t2 = v:locate(2,2)\n"
			"t3 = v:locate(0,0)\n"
			"t4 = v:locate(2.5,0)\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );

		lua_getglobal(L, "t1");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
		lua_getglobal(L, "t2");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
		lua_getglobal(L, "t3");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
		lua_getglobal(L, "t4");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
	}

	SECTION( "tri" ) {
		char const* script =
			"v = pgn:tri( 0, 0, 1, 0, 0, 1 )\n"
			"t1 = v:locate(1,1)\n"
			"t2 = v:locate(0.5,0.5)\n"
			"t3 = v:locate(0.25,0.25)\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );

		lua_getglobal(L, "t1");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
		lua_getglobal(L, "t2");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == 0 );
		lua_getglobal(L, "t3");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
	}

	// The following section checks the creation of a triangle with the
	// vertices specified in a clockwise direction.  This creates a triangular
	// hole in the plane.  It is not clear whether or not this is the desired
	// behaviour.
	/*SECTION( "tri" ) {
		char const* script =
			"v = pgn:tri( 0, 0, 0, 1, 1, 0 )\n"
			"t1 = v:locate(1,1)\n"
			"t2 = v:locate(0.5,0.5)\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );

		lua_getglobal(L, "t1");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == +1 );
		lua_getglobal(L, "t2");
		CHECK( lua_isnumber(L,-1) );
		CHECK( lua_tonumber(L,-1) == -1 );
	}*/

	SECTION( "gc" ) {
		char const* script =
			"v = pgn:rect(0,0,1,1)\n"
			"v = nil\n";

		lua_State* L = init_lua_state();
		int err = exec_lua_script( L, script );
		REQUIRE( err == 0 );
		lua_gc( L, LUA_GCCOLLECT, 0 );
	}
}
